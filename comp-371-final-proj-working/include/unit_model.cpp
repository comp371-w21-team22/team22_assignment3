#ifndef UNITMODEL_H // include guard
#define UNITMODEL_H   

#include <glm/glm.hpp>  // GLM is an optimized math library with syntax to similar to OpenGL Shading Language
#include <glm/gtc/matrix_transform.hpp> // include this to create transformation matrices
#include <glm/common.hpp>

#include "tools.cpp"

using namespace glm;

class UnitModel
{
public:
    UnitModel(int shaderProgram, mat4 initMatrix, int vertices, GLuint VAO, GLuint textureID)
    {

        m_shaderProgram = shaderProgram;
        m_drawMatrix = initMatrix;
        og_drawMatrix = initMatrix;
        m_vertices = vertices;

        m_VAO = VAO;
        m_textureID = textureID;
    }
    
    void transform(mat4 matrix)
    {
        // NOTE :
        // we need to apply the transform on the original matrix of the model (if use on the m_letter_id_matrix -> effect will be compounded)
        // hence why we have a og matrix
        m_drawMatrix = matrix * og_drawMatrix ;
    }
    
    void draw() {

        bind_texture();

        glBindVertexArray(m_VAO);

        setWorldMatrix(m_shaderProgram, m_drawMatrix);
        glDrawElements(GL_TRIANGLES, m_vertices, GL_UNSIGNED_INT, 0);

        glBindVertexArray(0);
    }

    void bind_texture(){
        // set texture
        glActiveTexture(GL_TEXTURE0);
        GLuint textureLocation = glGetUniformLocation(m_shaderProgram, "textureSampler");
        glBindTexture(GL_TEXTURE_2D, m_textureID);
        glUniform1i(textureLocation, 0);                // Set our Texture sampler to user Texture Unit 0
    }
    
private:
    GLuint mWorldMatrixLocation;
    mat4 m_drawMatrix;
    mat4 og_drawMatrix;
    int m_vertices;
    int m_shaderProgram;
    GLuint m_VAO;
    GLuint m_textureID;
};

// draw the given list of matrix
void draw_models(std::vector< UnitModel > umList){
    for(UnitModel um: umList){

        um.draw();
    }
}

class LightCube
{
public:
    LightCube(int shaderProgram, mat4 initMatrix, int vertices, GLuint VAO)
    {

        m_shaderProgram = shaderProgram;
        m_drawMatrix = initMatrix;
        og_drawMatrix = initMatrix;
        m_vertices = vertices;

        m_VAO = VAO;
    }
    
    void transform(mat4 matrix)
    {
        // NOTE :
        // we need to apply the transform on the original matrix of the model (if use on the m_letter_id_matrix -> effect will be compounded)
        // hence why we have a og matrix
        m_drawMatrix = matrix * og_drawMatrix ;
    }
    
    void draw() {

        glBindVertexArray(m_VAO);

        glUniformMatrix4fv(glGetUniformLocation(m_shaderProgram, "worldMatrix"), 1, GL_FALSE, &m_drawMatrix[0][0]);
        glDrawElements(GL_TRIANGLES, m_vertices, GL_UNSIGNED_INT, 0);

        glBindVertexArray(0);
    }

    
private:
    GLuint mWorldMatrixLocation;
    mat4 m_drawMatrix;
    mat4 og_drawMatrix;
    int m_vertices;
    int m_shaderProgram;
    GLuint m_VAO;
};

#endif /* UNITMODEL_H */
