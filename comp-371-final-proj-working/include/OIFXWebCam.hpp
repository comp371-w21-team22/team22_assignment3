//
// Created by Haotao Lai on 2018-08-01.
//

#ifndef OPENISS_FXWEBCAM_H
#define OPENISS_FXWEBCAM_H

#include <iostream>

#include "opencv2/opencv.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/imgproc/imgproc.hpp"

#include "OIDevice.hpp"

namespace openiss
{
class OIFrame;

class OIFXWebCam : public OIDevice
{

public:
    OIFXWebCam();
    ~OIFXWebCam();

    void* rawDevice() override;

    void init() override;
    void open() override;
    void close() override;

    bool enableColor() override;
    bool enableDepth() override;
    bool enableRegistered() override;
    bool enable() override;

    OIFrame* readFrame(StreamType typeFrame) override;
    Intrinsic getIntrinsic(StreamType streamType) override;
    Extrinsic getExtrinsic(StreamType from, StreamType to) override;
    
    float getDepthScale() override;

    // custom
    cv::Mat getFrame(int mode);

protected:

    cv::Mat frame; // frame that the webcam grabs
    cv::VideoCapture camera;


};

} // end of namespace

#endif // OPENISS_FXWEBCAM_H
