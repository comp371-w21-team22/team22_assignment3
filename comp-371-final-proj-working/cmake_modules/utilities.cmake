# Common utility functions for linking our libs and examples
# with common sets of dependencies.

function(debug_message part1 part2)
    if(is_debug)
        message(${part1} ${part2})
    endif(is_debug)
endfunction(debug_message)

function(link_basic_libs target)
    if(NITE2_FOUND)
        target_link_libraries(${target} ${NITE2_LIBRARIES})
        debug_message("${target}: link to nite2 libs -> " ${NITE2_LIBRARIES})
    endif()

    if(OPENNI2_FOUND)
        target_link_libraries(${target} ${OPENNI2_LIBRARIES})
        debug_message("${target}: link to openni2 libs -> " ${OPENNI2_LIBRARIES})
    endif()

    if(GLUT_FOUND)
        target_link_libraries(${target} ${GLUT_LIBRARIES})
        debug_message("${target}: link to glut libs -> " ${GLUT_LIBRARIES})
    endif()

    if(OPENGL_FOUND)
        target_link_libraries(${target} ${OPENGL_LIBRARIES})
        debug_message("${target}: link to OpenGL libs -> " ${OPENGL_LIBRARIES})
    endif()

    if(OpenCV_FOUND)
        include_directories(${OpenCV_INCLUDE_DIR})
        target_link_libraries(${target} ${OpenCV_LIBS})
        debug_message("${target}: link to opencv libs -> " ${OpenCV_LIBS})
    endif()

    if(REALSENSE_FOUND)
        target_link_libraries(${target} ${REALSENSE_LIBRARIES})
        debug_message("${target}: link to realsense libs -> " ${REALSENSE_LIBRARIES})
    endif(REALSENSE_FOUND)

    if(k4a_FOUND AND k4abt_FOUND)
        target_link_libraries(${target} k4a::k4a)
        target_link_libraries(${target} k4a::k4abt)
        debug_message("${target}: link to K4A libs -> " ${k4a_LIBRARY} ${k4abt_LIBRARY})
    endif()

    if(STRUCTURE_FOUND)
        include_directories(${STRUCTURE_INCLUDE_DIR})
        target_link_libraries(${target} ${STRUCTURE_LIBRARY})
        debug_message("${target}: link to structure libs -> " ${STRUCTURE_LIBRARY})
    endif(STRUCTURE_FOUND)
endfunction(link_basic_libs)


function(link_sample_basic_libs target)
    link_basic_libs(${target})

    if(OPENISS_FOUND)
        include_directories(${OPENISS_INCLUDE_DIRS})
        target_link_libraries(${target} ${OPENISS_LIBS})
        debug_message("${target}: link to openiss libs -> " ${OPENISS_LIBRARIES})
    endif()
endfunction(link_sample_basic_libs)


function(link_python_libs target)
    if(PYTHONLIBS_FOUND)
        debug_message("${target}: python include: " ${PYTHON_INCLUDE_DIRS})
        debug_message("${target}: python lib: " ${PYTHON_LIBRARIES})
        include_directories(${PYTHON_INCLUDE_DIRS})
        target_link_libraries(${target} ${PYTHON_LIBRARIES})
    endif(PYTHONLIBS_FOUND)

    if(PYTHON_NUMPY_FOUND)
        debug_message("${target}: numpy found: " ${PYTHON_NUMPY_INCLUDE_DIR})
        include_directories(${PYTHON_NUMPY_INCLUDE_DIR})
    endif(PYTHON_NUMPY_FOUND)
endfunction(link_python_libs)

# EOF
