###############################################################################
# Find Structure sdk
#
#     find_package(Structure)
#
# Variables defined by this module:
#
#  STRUCTURE_FOUND               True if Structure sdk was found
#  STRUCTURE_INCLUDE_DIRS        The location(s) of Structure sdk headers
#  STRUCTURE_LIBRARIES           Libraries needed to use Structure


find_path(
        STRUCTURE_INCLUDE_DIR CaptureSession.h
        /usr/local/include/ST /usr/include/ST $ENV{STRUCTURE_INCLUDE} ${STRUCTURE_INCLUDE}
)
find_library(
        STRUCTURE_LIBRARY Structure
        /usr/local/lib /usr/lib $ENV{STRUCTURE_LIBRARY}  ${STRUCTURE_LIBRARY}
)

set(STRUCTURE_INCLUDE_DIRS ${STRUCTURE_INCLUDE_DIR})
set(STRUCTURE_LIBRARIES ${STRUCTURE_LIBRARY})

if(STRUCTURE_INCLUDE_DIR AND STRUCTURE_LIBRARY)
    message("STRUCTURE_INCLUDE_DIRS and STRUCTURE_LIBRARY found")
    message("Structure libraries -> " ${STRUCTURE_LIBRARY})
    message("Structure headers -> " ${STRUCTURE_INCLUDE_DIRS})
    set(STRUCTURE_FOUND TRUE)
    include_directories(${STRUCTURE_INCLUDE_DIRS})

else(STRUCTURE_INCLUDE_DIR AND STRUCTURE_LIBRARY)
    message("STRUCTURE_INCLUDE_DIR AND STRUCTURE_LIBRARY not found")
endif(STRUCTURE_INCLUDE_DIR AND STRUCTURE_LIBRARY)