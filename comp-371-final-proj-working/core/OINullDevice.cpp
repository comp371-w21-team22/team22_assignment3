#include "OINullDevice.hpp"

#include <iostream>
#include <cstring>

namespace openiss
{

#define OPENISS_DEFAULT_BYTES_PER_DEPTH_PIXEL 2
#define OPENISS_DEFAULT_BYTES_PER_COLOR_PIXEL 3

OINullDevice::OINullDevice()
        :OIDevice()
{
    init();
}

OINullDevice::~OINullDevice()
{
    // Make sure the parent destructor does not double-free
    this->mpDepthOIFrame = nullptr;
    this->mpColorOIFrame = nullptr;
    this->mpIrOIFrame = nullptr;

    // Deallocate our own resources
    if(m_poGeneratedDepth != nullptr)
    {
        free(m_poGeneratedDepth->getData());
    	delete m_poGeneratedDepth;
    }

    if(m_poGeneratedColor != nullptr)
    {
        free(m_poGeneratedColor->getData());
    	delete m_poGeneratedColor;
    }

    if(m_poGeneratedIR != nullptr)
    {
        free(m_poGeneratedIR->getData());
    	delete m_poGeneratedIR;
    }
}


void OINullDevice::init()
{
    if(this->m_bInitialized == false)
    {
        // Populate some test fixture data

        void* l_pacDepthData = malloc
        (
            OPENISS_DEFAULT_BYTES_PER_DEPTH_PIXEL
            * OPENISS_DEFAULT_NULL_DEVICE_WIDTH
            * OPENISS_DEFAULT_NULL_DEVICE_HEIGHT
        );
/*
        // Just populate something at the depth frame
        for
        (
            int i = 0;
            i < OPENISS_DEFAULT_NULL_DEVICE_WIDTH * OPENISS_DEFAULT_NULL_DEVICE_HEIGHT;
            i += OPENISS_DEFAULT_BYTES_PER_DEPTH_PIXEL
        )
        {
            ((unsigned int*)l_pacDepthData)[i] = (i % 16000);
        }
//*/
        // Properly formatted depth image is 16 bit:
        // K1: 11bits depth, 5 user: 00000000111 00001 = 225
        // K2: 12bits depth, 4 user: 000000001110 0001
        // https://stackoverflow.com/questions/13162839/kinect-depth-image
        // Was abitrary 128
        // TODO: revert to the above loop instead to generate some gradient with one user
        memset(l_pacDepthData, 225,
            OPENISS_DEFAULT_BYTES_PER_DEPTH_PIXEL
            * OPENISS_DEFAULT_NULL_DEVICE_WIDTH
            * OPENISS_DEFAULT_NULL_DEVICE_HEIGHT
        );

        m_poGeneratedDepth = new OIDataFrame
        (
            FrameType::DEPTH_FRAME,
            l_pacDepthData,
            OPENISS_DEFAULT_BYTES_PER_DEPTH_PIXEL,
            OPENISS_DEFAULT_NULL_DEVICE_WIDTH,
            OPENISS_DEFAULT_NULL_DEVICE_HEIGHT
        );

        void* l_pacColorData = malloc
        (
            OPENISS_DEFAULT_BYTES_PER_COLOR_PIXEL
            * OPENISS_DEFAULT_NULL_DEVICE_WIDTH
            * OPENISS_DEFAULT_NULL_DEVICE_HEIGHT
        );

/*
        // Populate something in the color frame as a function of dept
        for
        (
            int i = 0, j = 0;
            i < OPENISS_DEFAULT_NULL_DEVICE_WIDTH * OPENISS_DEFAULT_NULL_DEVICE_HEIGHT,
            j < OPENISS_DEFAULT_NULL_DEVICE_WIDTH * OPENISS_DEFAULT_NULL_DEVICE_HEIGHT;
            i += OPENISS_DEFAULT_BYTES_PER_COLOR_PIXEL,
            j += OPENISS_DEFAULT_BYTES_PER_DEPTH_PIXEL
        )
        {
            ((unsigned int*)l_pacColorData)[i] = ((unsigned char*)l_pacDepthData)[j] + i;
        }
//*/
        memset(l_pacColorData, 255,
            OPENISS_DEFAULT_BYTES_PER_COLOR_PIXEL
            * OPENISS_DEFAULT_NULL_DEVICE_WIDTH
            * OPENISS_DEFAULT_NULL_DEVICE_HEIGHT
        );

        m_poGeneratedColor = new OIDataFrame
        (
            FrameType::COLOR_FRAME,
            l_pacColorData,
            OPENISS_DEFAULT_BYTES_PER_COLOR_PIXEL,
            OPENISS_DEFAULT_NULL_DEVICE_WIDTH,
            OPENISS_DEFAULT_NULL_DEVICE_HEIGHT
        );

        void* l_pacIRData = malloc
        (
            OPENISS_DEFAULT_BYTES_PER_COLOR_PIXEL
            * OPENISS_DEFAULT_NULL_DEVICE_WIDTH
            * OPENISS_DEFAULT_NULL_DEVICE_HEIGHT
        );

/*
        // Assume for simplity IR = Color XOR Depth
        for
        (
            int i = 0, j = 0;
            i < OPENISS_DEFAULT_NULL_DEVICE_WIDTH * OPENISS_DEFAULT_NULL_DEVICE_HEIGHT,
            j < OPENISS_DEFAULT_NULL_DEVICE_WIDTH * OPENISS_DEFAULT_NULL_DEVICE_HEIGHT;
            i += OPENISS_DEFAULT_BYTES_PER_COLOR_PIXEL,
            j += OPENISS_DEFAULT_BYTES_PER_DEPTH_PIXEL
        )
        {
            //((unsigned int*)l_pacIRData)[i] = i;
                //((unsigned int*)l_pacColorData)[i] ^ i;
                //((unsigned int*)l_pacDepthData)[j];
        }
//*/
        memset(l_pacIRData, 176,
            OPENISS_DEFAULT_BYTES_PER_COLOR_PIXEL
            * OPENISS_DEFAULT_NULL_DEVICE_WIDTH
            * OPENISS_DEFAULT_NULL_DEVICE_HEIGHT
        );

        m_poGeneratedIR = new OIDataFrame
        (
            FrameType::IR_FRAME,
            l_pacIRData,
            OPENISS_DEFAULT_BYTES_PER_COLOR_PIXEL,
            OPENISS_DEFAULT_NULL_DEVICE_WIDTH,
            OPENISS_DEFAULT_NULL_DEVICE_HEIGHT
        );

        std::cout << "OpenISS null device initialized..." << std::endl;
        this->m_bInitialized = true;
    }
    else
    {
        std::cerr << "OpenISS null device has already been initialized..." << std::endl;
    }
}

void OINullDevice::close()
{
    std::cout << "OpenISS null device closed..." << std::endl;
}

void OINullDevice::open()
{
    std::cout << "OpenISS null device opened..." << std::endl;
}

OIFrame* OINullDevice::readFrame(StreamType typeFrame)
{
    switch (typeFrame)
    {
        case COLOR_STREAM:
            return mpColorOIFrame;
            break;
        
        case DEPTH_STREAM:
            return mpDepthOIFrame;
            break;

        case IR_STREAM:
            return mpIrOIFrame;
            break;

        default:
            std::cerr
                << "[OpenISS][debug] not supported stream type "
                << typeFrame
                << std::endl;
            return nullptr;
            break;
    }
}

void* openiss::OINullDevice::rawDevice()
{
   return this;
}

bool OINullDevice::enableColor()
{
    if(!m_is_enable_rgb)
    {
        m_is_enable_rgb = true;
        mpColorOIFrame = m_poGeneratedColor;
    }

    return m_is_enable_rgb;
}

bool OINullDevice::enableDepth()
{
    if(!m_is_enable_depth)
    {
        m_is_enable_depth = true;
        mpDepthOIFrame = m_poGeneratedDepth;
    }

    return m_is_enable_depth;
}

// TODO: do not mix registered and IR
bool OINullDevice::enableRegistered()
{
    if (!m_is_enable_registered)
    {
        m_is_enable_registered = true;
        mpIrOIFrame = m_poGeneratedIR;
    }

    return m_is_enable_registered;
}

Intrinsic OINullDevice::getIntrinsic(StreamType streamType)
{
    return Intrinsic();
}

Extrinsic OINullDevice::getExtrinsic(StreamType from, StreamType to)
{
    return Extrinsic();
}

float OINullDevice::getDepthScale()
{
    return 0;
}

} // namespace

// EOF
