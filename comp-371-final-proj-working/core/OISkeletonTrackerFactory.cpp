/**
 * @author Serguei Mokhov
 * @author Initially created by Haotao Lai on 2018-08-10.
 */

#include <iostream>
#include <cstring>

#include "OINullSkeletonTracker.hpp"

#ifdef OPENISS_NITE2_SUPPORT
#include <NiTE.h>
#include "OINiTESkeletonTracker.hpp"
#endif

using namespace openiss;

OITrackerFactory::~OITrackerFactory()
{

}

// TODO: should not use "names", instead should refactor to use enums

std::shared_ptr<OISkeletonTracker>
OITrackerFactory::createTracker(const char* tracName, OIDevice* pDev)
{
    // Default is a NULL tracker
    if(strcmp("null", tracName) == 0 || tracName == nullptr)
    {
        std::shared_ptr<OISkeletonTracker> tracker(new OINullSkeletonTracker(*pDev));
        return tracker;
    }

#ifdef OPENISS_NITE2_SUPPORT
    // TODO: unsafe string comparison
    if(strcmp(tracName, "nite") == 0)
    {
        // TODO: This NITE init should be in the tracker, not in the factory.
        if (!mIsNiteInit)
        {
            nite::NiTE::initialize();
            mIsNiteInit = true;
        }

        std::shared_ptr<OISkeletonTracker> tracker(new OINiTESkeletonTracker(*pDev));
        return tracker;
    }
#endif // OPENISS_NITE2_SUPPORT

    std::cerr
        << "OITrackerFactory: could not create any tracker ("
        << tracName << ")"
        << std::endl;

    return nullptr;
}

shared_ptr<OITrackerFrame>
OITrackerFactory::createTrackerFrame(const char* tracName)
{
    // Default is a NULL tracker
    if(strcmp("null", tracName) == 0 || tracName == NULL)
    {
        std::shared_ptr<OITrackerFrame> frame(new OINullTrackerFrame);
        return frame;
    }

#ifdef OPENISS_NITE2_SUPPORT
    if (strcmp(tracName, "nite") == 0)
    {
        std::shared_ptr<OITrackerFrame> frame(new OINiTETrackerFrame);
        return frame;
    }
#endif // OPENISS_NITE2_SUPPORT

    std::cerr
        << "OITrackerFactory: could not create any tracker frame ("
        << tracName << ")"
        << std::endl;

    return nullptr;
}

// EOF
