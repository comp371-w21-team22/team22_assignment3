#include "OIGestureTracker.hpp"
#include "OINullSkeletonTracker.hpp"

namespace openiss
{

// OITracker API

void OIGestureTracker::startTracking()
{
    OIStatus l_eStatus = startGestureDetection();

    if(l_eStatus != OIStatus::STATUS_OK)
    {
        std::cerr
            << "OIGestureTracker::startTracking(): failed to start gesture detection..."
            << " (" << static_cast<int>(l_eStatus) << ")"
            << std::endl;
    }

    l_eStatus = startHandTracking();

    if(l_eStatus != OIStatus::STATUS_OK)
    {
        std::cerr
            << "OIGestureTracker::startTracking(): failed to start hand tracking..."
            << " (" << static_cast<int>(l_eStatus) << ")"
            << std::endl;
    }
}

void OIGestureTracker::stopTracking()
{
    OIStatus l_eStatus = stopGestureDetection();

    if(l_eStatus != OIStatus::STATUS_OK)
    {
        std::cerr
            << "OIGestureTracker::stopTracking(): failed to stop gesture detection..."
            << " (" << static_cast<int>(l_eStatus) << ")"
            << std::endl;
    }

    l_eStatus = stopHandTracking();

    if(l_eStatus != OIStatus::STATUS_OK)
    {
        std::cerr
            << "OIGestureTracker::stopTracking(): failed to stop hand tracking..."
            << " (" << static_cast<int>(l_eStatus) << ")"
            << std::endl;
    }
}

OITrackerFrame* OIGestureTracker::readFrame(OITrackerFrame* p_poTrackerFrame = nullptr)
{
    std::cerr
        << "OIGestureTracker::readFrame() has not been fully implemented..." << std::endl
        << "  --> returning passed frame if not null or creating a null tracker frame..."
        << std::endl;

    if(p_poTrackerFrame != nullptr)
    {
        std::cerr << "  --> returning parameter tracker frame..."  << std::endl;
        return p_poTrackerFrame;
    }
    else
    {
        std::cerr << "  --> returning new null tracker frame..."  << std::endl;
        return new OINullTrackerFrame();
    }
}

/* Return available gestures. */
std::vector<OIGestureData> OIGestureTracker::getGestures()
{
    return m_eGestureData;
}

/* Return available hands. */
std::vector<OIHandData> OIGestureTracker::getHands()
{
    return m_eHandData;
}

} // namespace

// EOF
