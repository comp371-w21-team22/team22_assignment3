
#include "OINullGestureTracker.hpp"
#include "OIType.hpp"

using namespace openiss;

/* Constructor */
OINullGestureTracker::OINullGestureTracker():
    m_bInitialized(false),
    m_bGestureDetectionRunning(false),
    m_bHandTrackingRunning(false)
{
    // TODO: refactor up
#ifdef OPENISS_INSTRUMENTATION
    m_depth_callback_counter = 0;
    m_gesture_callback_counter = 0;
    m_hand_callback_counter = 0;
#endif

    m_eStatus = OIStatus::STATUS_OK;
}

/* Destructor */
OINullGestureTracker::~OINullGestureTracker()
{
    stop();
}

/* Initialize Tracker */
OIStatus OINullGestureTracker::init()
{
    // Stub fake "detected" gesture for testing
    this->m_eGesture.setGesturePosition(10, 10, 10);
    this->m_eGesture.setGestureType(OIGestureType::GESTURE_WAVE);
    this->m_eGesture.setGestureState(OIGestureState::GESTURE_IS_COMPLETE);

    this->m_eGestureData.push_back(this->m_eGesture);

    this->m_eHand.setHandID(0);
    this->m_eHand.setHandPosition(20, 10, 10);
    this->m_eHand.setHandState(OIHandState::HAND_IS_NEW);

    this->m_eHandData.push_back(this->m_eHand);

    // TODO: kludge
    m_poDepthFrame = new OIDepthFrame();
    void* l_pacDepthData = malloc(2 * 640 * 480);
    memset(l_pacDepthData, 128, 2 * 640 * 480);
    m_poDepthFrame->setDepthData((const uint16_t*)l_pacDepthData);
    m_poDepthFrame->setWidth(640);
    m_poDepthFrame->setHeight(480);
    // END kludge

    m_bInitialized = true;

#ifdef OPENISS_DEBUG
    std::cout << "OINullGestureTracker::init() complete..." << std::endl;
#endif

    return m_eStatus;
}

/* Stop Tracker */
OIStatus OINullGestureTracker::stop()
{
    this->m_bInitialized = false;
    free((void*)m_poDepthFrame->getDepthData());
    delete m_poDepthFrame;
    return m_eStatus;
}

/* Update Tracker data */
OIStatus OINullGestureTracker::update()
{
    if(this->m_bInitialized == true)
    {
        this->m_eHandData[0].setHandState(OIHandState::HAND_IS_TRACKING);
    }

    return m_eStatus;
}

/* Start gesture detection. */
OIStatus OINullGestureTracker::startGestureDetection()
{
    this->m_bGestureDetectionRunning = true;
    std::cout << "OINullGestureTracker: Started gesture detection..." << std::endl;
    return m_eStatus;
}

/* Stop gesture detection. */
OIStatus OINullGestureTracker::stopGestureDetection()
{
    this->m_bGestureDetectionRunning = false;
    std::cout << ":OINullGestureTracker: Stopped gesture detection..." << std::endl;
    return m_eStatus;
}

/* Start Hand Tracking. */
OIStatus OINullGestureTracker::startHandTracking()
{
    this->m_bHandTrackingRunning = true;
    std::cout << "OINullGestureTracker: Hand tracking started..." << std::endl;
    return m_eStatus;
}

/* Stop Hand Tracking. */
OIStatus OINullGestureTracker::stopHandTracking()
{
    this->m_bHandTrackingRunning = false;
    std::cout << "OINullGestureTracker: Hand tracking stopped..." << std::endl;
    return m_eStatus;
}

/* Return most recent frame. */
// TODO -- refactor away
OIDepthFrame* OINullGestureTracker::getDepthFrame()
{
    return m_poDepthFrame;
}

/* Real coordinates to Projective Coordinates*/
void OINullGestureTracker::convertHandCoordinatesToDepth(float p_x, float p_y, float p_z, float *p_OutX, float *p_OutY)
{
    // For now just assume what is passed is returned back
    memcpy(p_OutX, &p_x, sizeof(float));
    memcpy(p_OutY, &p_y, sizeof(float));
}

/* Projective coordinates to Real Coordinates*/
void OINullGestureTracker::convertDepthCoordinatesToHand(int p_x, int p_y, int p_z, float *p_OutX, float *p_OutY)
{
    // For now just assume what is passed is returned back
    *p_OutX = 0;
    *p_OutY = 0;

    memcpy(p_OutX, &p_x, sizeof(int));
    memcpy(p_OutY, &p_y, sizeof(int));
}

// EOF