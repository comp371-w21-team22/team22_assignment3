//
// Created by Haotao Lai on 2018-08-17.
//

#include <utility>

#include "OIUser.hpp"

using namespace openiss;

// ////////////////////////////////////////////////////////////////////////////
// OIUserData class implementation
// ////////////////////////////////////////////////////////////////////////////
OIUserData::OIUserData(long id)
  : mUserId(id)
{
    mpSkeleton = new OISkeleton;
    this->m_poFace = new OIFace();
}

OIUserData::~OIUserData() 
{
    delete this->mpSkeleton;
    delete this->m_poFace;
    
    this->m_aoGestures.clear();
    this->m_aoHands.clear();
}

OISkeleton* OIUserData::getSkeleton() const
{
    return mpSkeleton;
}

OIFace* OIUserData::getFace() const
{
    return this->m_poFace;
}

std::vector<OIGestureData> OIUserData::getGestures() const
{
    return this->m_aoGestures;
}

std::vector<OIHandData> OIUserData::getHands() const
{
    return this->m_aoHands;
}

long OIUserData::getID() const
{
    return mUserId;
}

Point3f* OIUserData::getBoundingBox()
{
    return boundingBox;
}

Point3f* OIUserData::getCenterOfMass()
{
    return &centerPoint;
}


// ////////////////////////////////////////////////////////////////////////////
// OIUserMap class implementation
// ////////////////////////////////////////////////////////////////////////////

// TODO: move to utils/tools
static void copyPtrArray(const short* src, int len, short* dst)
{
    // TODO: document caller is responsible for free/delete
    dst = new short[len];

    // TODO: not efficient, use memcpy
    for (int i = 0; i < len; i++)
    {
        dst[i] = src[i];
    }
}

void OIUserMap::_copy(const OIUserMap& otherUserMap)
{
    mWidth = otherUserMap.getWidth();
    mHeight = otherUserMap.getHeight();
    copyPtrArray(otherUserMap.getPixels(), mWidth * mHeight, mpUserArray);
}

OIUserMap::OIUserMap(int width, int height)
    : mWidth(width)
    , mHeight(height)
    , mpUserArray(nullptr)
{

}

OIUserMap::OIUserMap(int width, int height, short* map)
        : mWidth(width)
        , mHeight(height)
        , mpUserArray(nullptr)
{
    copyPtrArray(map, width * height, mpUserArray);
}

OIUserMap::OIUserMap(const OIUserMap& otherUserMap)
{
    _copy(otherUserMap);
}

OIUserMap::~OIUserMap()
{
    delete[] mpUserArray;
}

OIUserMap& OIUserMap::operator=(const OIUserMap& otherMap)
{
    _copy(otherMap);
    return *this;
}

int OIUserMap::getHeight() const
{
    return mHeight;
}

int OIUserMap::getWidth() const
{
    return mWidth;
}

short* OIUserMap::getPixels() const
{
    return mpUserArray;
}

// EOF
