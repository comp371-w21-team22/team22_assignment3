//
// Created by jash on 2019-12-16.
//
#include "OIUtils.hpp"

#ifdef ENABLE_OPENISS_SDL
#include <SDL2/SDL.h>
#endif

using namespace openiss;

FPSCounter::FPSCounter() :
        start_time(0),
        pause_time(0),
        paused(false),
        started(false) { }

void FPSCounter::Start()
{
    started = true;
    paused = false;
#ifdef ENABLE_OPENISS_SDL
    start_time = SDL_GetTicks();
#endif
    pause_time = 0;
}

void FPSCounter::Stop()
{
    started = false;
    paused = false;
    start_time = 0;
    pause_time = 0;
}

void FPSCounter::Pause()
{
    if (started && !paused) {
        paused = true;
#ifdef ENABLE_OPENISS_SDL
        pause_time = SDL_GetTicks() - start_time;
#endif
        start_time = 0;
    }
}

void FPSCounter::Play()
{
    if (started && paused) {
        paused = true;
        pause_time = 0;
#ifdef ENABLE_OPENISS_SDL
        start_time = SDL_GetTicks() - pause_time;
#endif
    }
}

uint32_t FPSCounter::GetTicks()
{
    uint32_t time = 0;
    if (started) {
        if (paused) {
            time = pause_time;
        }
        else {
#ifdef ENABLE_OPENISS_SDL
            time = SDL_GetTicks() - start_time;
#endif
        }
    }

    return time;
}

bool FPSCounter::IsStarted() { return started; }
bool FPSCounter::IsPaused() { return paused && started; }

FPSCounter::~FPSCounter() { }
