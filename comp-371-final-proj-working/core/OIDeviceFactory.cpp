//
// Created by Haotao Lai on 2018-08-09.
//

#include "OIDevice.hpp"
#include "OINullDevice.hpp"

#include <iostream>
#include <cstring>

#ifdef OPENISS_OPENNI2_SUPPORT
#include "OIKinect.hpp"
#endif

#ifdef OPENISS_REALSENSE_SUPPORT
#include "OIRealSenseD435.hpp"
#endif

#ifdef OPENISS_K4A_SUPPORT
#include "OIKinectAzure.hpp"
#endif

#ifdef OPENISS_STRUCTURE_SUPPORT
#include "OIStructure.hpp"
#endif

std::shared_ptr<openiss::OIDevice> openiss::OIDeviceFactory::create(const char* p_pcDevName)
{
    if (strcmp(p_pcDevName, "null") == 0 || p_pcDevName == NULL)
    {
#ifdef OPENISS_DEBUG
        std::cerr << "OIDeviceFactory: creating null device..." << std::endl;
#endif

        std::shared_ptr<OIDevice> l_poDev(new OINullDevice);

#ifdef OPENISS_DEBUG
        std::cerr << "OIDeviceFactory: null device created!" << std::endl;
#endif

        return l_poDev;
    }

#ifdef OPENISS_OPENNI2_SUPPORT
    if (strcmp(p_pcDevName, "kinect") == 0)
    {
        // TODO: refactor away; should be done in the device wrapper
        if (!mIsOpenNIInit)
        {
            openni::OpenNI::initialize();
            mIsOpenNIInit = true;
        }

        std::shared_ptr<OIDevice> l_poDev(new OIKinect);
        return l_poDev;
    }
#endif

#ifdef OPENISS_REALSENSE_SUPPORT
    else if (std::strcmp(p_pcDevName, "rs_d435") == 0)
    {
        std::shared_ptr<OIDevice> l_poDev(new OIRealSenseD435);
        return l_poDev;
    }
#endif

#ifdef OPENISS_K4A_SUPPORT
    else if (std::strcmp(p_pcDevName, "k4a") == 0)
    {
        std::shared_ptr<OIDevice> l_poDev(new OIKinectAzure);
        return l_poDev;
    }
#endif

#ifdef OPENISS_STRUCTURE_SUPPORT
    else if (std::strcmp(p_pcDevName, "structure") == 0)
    {
        std::shared_ptr<OIDevice> l_poDev(new OIStructure);
        return l_poDev;
    }
#endif
   
    else
    {
        std::cerr
            << "OIDevice: cannot create a device for \'" << p_pcDevName << "\'. "
            << "It is either unsupported or unknown or not compiled in."
            << std::endl;

        return nullptr;
    }
}

// EOF
