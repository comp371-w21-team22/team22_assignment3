//
// Created by Haotao Lai on 2018-10-24.
//

#include "OIDataFrame.hpp"

#include <iostream>

#include <cstring> // memcpy()
#include <cassert> // assert()

#ifdef OPENISS_OPENCV_SUPPORT
#include "OIFrameCVImpl.hpp"
#endif

void openiss::OIDataFrame::createDisplayImg()
{
#ifdef OPENISS_OPENCV_SUPPORT
    // TODO: switch/case
    if (type == FrameType::DEPTH_FRAME)
    {
        cv::Mat img(height, width, CV_16U, mpData);
        displayCVImpl = new OIFrameCVImpl(img);
    }
    else if (type == FrameType::COLOR_FRAME)
    {
        cv::Mat img(height, width, CV_8UC3, mpData);
        displayCVImpl = new OIFrameCVImpl(img);
    }
    else if (type == FrameType::IR_FRAME)
    {
        cv::Mat img(height, width, CV_8UC1, mpData);
        displayCVImpl = new OIFrameCVImpl(img);
    }
    else
    {
        std::cerr << "Unsupported frame type: " << type << std::endl;
        assert("not supported frame type");
    }
#else
    std::cerr
        << "OIDataFrame:createDisplayImg(): OpenCV display code is not compiled in for frame type "
        << type
        << std::endl;
#endif
}

openiss::OIDataFrame::OIDataFrame(FrameType type, void* data, int bpp, int width, int height)
  : type(type), bpp(bpp), width(width), height(height), mpData(data),
  displayCVImpl(nullptr), hasOwnData(false), hasDispImg(false)
{

}

openiss::OIDataFrame::OIDataFrame(openiss::OIDataFrame& frame)
  : type(frame.type), bpp(frame.getBytesPerPixel()), width(frame.getWidth()), height(frame.getHeight()),
  displayCVImpl(nullptr), hasOwnData(true), hasDispImg(false)
{
    int len = width * height * bpp;
    mpData = new uint8_t[len];

    memcpy(mpData, frame.getData(), static_cast<size_t>(len));
}

void* openiss::OIDataFrame::getData() const 
{
    return mpData;
}

void openiss::OIDataFrame::setData(void* p_paData)
{
   this->mpData = p_paData;
}

openiss::OIFrame* openiss::OIDataFrame::getFrame()
{
    if (!hasDispImg) { createDisplayImg(); }
    return displayCVImpl;
}

int openiss::OIDataFrame::getBytesPerPixel() const
{
    return bpp;
}

int openiss::OIDataFrame::getHeight() const
{
    return height;
}

int openiss::OIDataFrame::getWidth() const
{
    return width;
}

void openiss::OIDataFrame::save(std::string fileName)
{
    if (!hasDispImg)
    {
        createDisplayImg();
    }

    displayCVImpl->save(fileName);
}

void openiss::OIDataFrame::save(std::string path, std::string fileName) {
    if (!hasDispImg) { createDisplayImg(); }
    displayCVImpl->save(path, fileName);
}

void openiss::OIDataFrame::show(const char* winName)
{
    if (!hasDispImg)
    {
        createDisplayImg();
    }

    displayCVImpl->show(winName);
}

void openiss::OIDataFrame::drawSkeleton(openiss::OISkeleton* pSkeleton, std::vector<JointType>& types)
{
    if(!hasDispImg)
    {
        createDisplayImg();
    }

    displayCVImpl->drawSkeleton(pSkeleton, types);
}

openiss::OIDataFrame::~OIDataFrame()
{
    if (hasOwnData)
    {
        // TODO: switch/case
        if (type == FrameType::DEPTH_FRAME) {
            delete[] (uint16_t *) mpData;
        }
        else if (type == FrameType::COLOR_FRAME) {
            delete[] (uint8_t *) mpData;
        }
        else if (type == FrameType::IR_FRAME) {
            delete[] (uint8_t *) mpData;
        }
        else {
            assert("cannot delete memory with void type");
        }
    }
    delete displayCVImpl;
}

// TODO: refactor away
#ifdef OPENISS_OPENCV_SUPPORT
cv::Mat openiss::OIDataFrame::getOpencvMat()
{
    return displayCVImpl->getOpencvMat();
}
#endif
