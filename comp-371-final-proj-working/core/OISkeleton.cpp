//
// Created by Haotao Lai on 2018-08-23.
//

#include <iostream>

#include "OISkeleton.hpp"

// /////////////////////////////////////////////////////////////////////////////
// OISkeleton class implementation
// /////////////////////////////////////////////////////////////////////////////

using namespace openiss;

void OISkeleton::addJointByType(JointType type, shared_ptr<OISkeletonJoint> jointPtr)
{
    auto search = mJoints.find(type);
    
    if(search != mJoints.end())
    {
        mJoints.erase(type);
    }

    mJoints.insert({type, jointPtr});

    //std::cout << "OISkeleton::addJointByType(" << type << "), added: " << jointPtr << std::endl;
}

shared_ptr<OISkeletonJoint> OISkeleton::getJointByType(JointType type)
{
    //std::cout << "OISkeleton::getJointByType(" << type << ")" << std::endl;

    auto res = mJoints.find(type);
    
    if(res != mJoints.end())
    {
        return res->second;
    }

    //std::cout << "OISkeleton::getJointByType(" << type << ") --- NOT FOUND!" << std::endl;

    return nullptr;
}

void OISkeleton::drawToFrame(OIFrame* displayFrame, std::vector<JointType>& supportedJoints)
{
    displayFrame->drawSkeleton(this, supportedJoints);
}

void OISkeleton::mapWorld2Image(OISkeletonTracker* tracker)
{
    for(const auto &entry : mJoints)
    {
        const shared_ptr<OISkeletonJoint>& joint = entry.second;
        tracker->mapJoint2Depth(joint->x, joint->y, joint->z, &(joint->row), &(joint->col));
    }
}

void OISkeleton::setSkeletonState(bool isAvailable)
{
    mIsSkeletonAvailable = isAvailable;
}

bool OISkeleton::getSkeletonState() const
{
    return mIsSkeletonAvailable;
}

// /////////////////////////////////////////////////////////////////////////////
// OISkeletonJoint class definition
// /////////////////////////////////////////////////////////////////////////////

OISkeletonJoint::OISkeletonJoint(float x, float y, float z, JointType t, float row, float col)
    : type(t), x(x), y(y), z(z), row(row), col(col)
{

}
