# README

This is an OpenISS C++ framework with different build options
for different backends and as a library (static and dynamic).

## Milestones and TODO

### Current ###

- See the issue tracker
- Integration (2019-2020)
  - Gesture framework
  - Unity plugin
  - Structure and Azure Kinect devices
  - Null Framework
  - Pd and Max plugins
  - v0.1.1-2020-iReid (merge of the ReID framework/etc and refactoring)
  - v0.1.1-2020-iFace (merge of the Face framework and refactoring)
  - v0.1.1-2020-iNull (introduction of a basic Null framework and refactoring)

### Legacy ###

- 2018-08-30, we are able to do skeleton tracking via OpenISS API
- 2018-08-09, we can get both color and depth images from both Kinect v1 and v2
  via a common interface (in the other words, the same code can be used for both two types of devices).

## Build instructions

All code here is compiled by `CMake`, the minimum version
requirement is `3.12`, if you don't have can download from
[here](https://cmake.org/download/).

*If you really don't want to upgrade. It has been tested that can work with `CMake 3.8`
with some modifications.*

- For pure framework build without dependencies and using
  `OINuillDevice` and `OINullSkeletonTracker` API for quick tests,
  enable the `BUILD_CORE_FRAMEWORK_ONLY` cmake option (enabled by default).
- The instructions below take `_example` or `ogl` tutorial samples as an examples. You can
  try runing the program with or without Kinects v1, v2, RealSense, or Structure. By default none of the devices are required and the Null Framework is used instead. It requires additional
  dependencies and flags, usually installed via installtion
  scripts under `src/scripts/`.
  - Alternatively need to use our docker image, currently set for Ubuntu.

### Install needed libraries

For the **Null Framework**-based testing most of the below
libraries are not needed. Add OpenGL and OpenCV for convenience.

For a **complete** build make sure you have all the relevant libraries installed:

- [OpenNI2](https://github.com/occipital/OpenNI2) (some distributions, like Nuitrack bundle it as well)
- [NiTE2](https://bitbucket.org/kaorun55/openni-2.2/src/master/NITE%202.2%20%CE%B1/) (older tracking middleware)
- [OpenCV](https://opencv.org/releases.html) (Not useful for `_example` or `ogl` tutorials,
   but we need it later, build instructions for Ubuntu
   can be found [here](https://www.learnopencv.com/install-opencv3-on-ubuntu/))
- [Freenect2](https://github.com/OpenKinect/libfreenect2) (should be built
   with OpenNI2 supported; supports Kinect v2)
- [Freenect](https://github.com/OpenKinect/libfreenect) (supports Kinect v1)
- `libfrealsense2` - for RealSense cameras support
- OpenGL 2 and 3+ for various rendered examples
- ROS - for ROS eco system support
- OpenPose - for the corresponding backend
- OpenFace - for the corresponding backend
- Nuitrack - for the corresponding backend
- Unity SDK - for the corresponding backend
- .NET - for the C# wrapper
- Blender - for outputting into a Blender plugin (via libfreenect2)
- PCL - for point clouds
- TensorFlow - for DL works on re-identification and facial emotion/landmarks recognition
- openFrameworks - for collection of common creative coding C++ classes and wrappers
- tinyosc - for OSC connectivity

Some of these are included as git submodules within
OpenISS root directory or can be installed using a package manager
for your OS or using one of our build scripts.

If you are on a Mac, make sure you have the OpenNI2 and NiTE2 shared 
libraries (or symbol link) under path: `/usr/lib/` (**if in production**).

How to build the above libs from source code, please refer to the specific
project website for instructions or use our build scripts if your platform is there.
Need to note that when you finish building
the `freenect` (or `freenect2`) lib, need to copy the `libfreenect2-openni2.0.dylib`
(take `freenect2` as an example) file to the directory `your_openni2_lib_path/OpenNI2/Driver/`.
The build scripts can be used for CI and Docker builds as well.

After you get all those libs, you need to manually copy the `NiTE2/` folder of
NiTE2 lib which contains the pre-trained data into the root directory of the
current project, currently located in `models/NiTE`.

#### ML Models

Deep learning-based models are maintained in other places listed below:

- [person detection model](https://bitbucket.org/eric_thesis/yolov3/src/master/)
- [person re-identification model](https://bitbucket.org/eric_thesis/reid-tfk/src/master/)

### Environment variables

If you get a error like some libs cannot be found, for example, OpenNI or NiTE.
A good way to solve it is define some environment variables. In the cmake moudle
files, it will search for the following environment vars:

- `NITE2_INCLUDE`
- `NITE2_REDIST`
- `OPENNI2_INCLUDE`
- `OPENNI2_REDIST`

Those variables can be accessed after you run the `install.sh` script comes with the
lib's repo. After running the script, you should find a file in the same folder with 
the name `*EnvironmentVariable`.

Bash command to add environment variables:
```
export NITE2_INCLUDE=your_nite_include_path
```

For example, the following variables were set in an *Ubuntu 16.04* environment.

**OPENNI2 Environment Variables**
1. `$ export OPENNI2_INCLUDE=/home/$USER/OpenNI-Linux-x64-2.2/Include`
2. `$ export OPENNI2_REDIST=/home/$USER/OpenNI-Linux-x64-2.2/Redist`

**NiTE2 Environment Variables**
1. `$ export NITE2_INCLUDE=/home/$USER/NiTE-Linux-x64-2.2/Include`
2. `$ export NITE2_REDIST64=/home/$USER/NiTE-Linux-x64-2.2/Redist`

**NuiTrack Environment Variables**
1. `$ export NUITRACK_INCLUDE=/home/$USER/Nuitrack/include`
2. `$ export NUITRACK_REDIST=/home/$USER/Nuitrack/lib/linux64`

These environment variables can be added to **~/.bashrc** for persistance.

*NOTE: Kinect v1 1473, OpenNI2 and libfreenect will work together only if libfreenect is compiled with OpenNI2 suppport enabled which yields libFreenectDriver.so which needs to be copied to the OPENNI2_REDIST/OpenNI2/Drivers/ More info at [FreenectDriver](https://github.com/OpenKinect/libfreenect/tree/master/OpenNI2-FreenectDriver)*


### Build

After exclusively successfully completing the pre-requisites, clone the repo and
then do the following:

1. `cd` into the root directory of the `cpp` project -- `cd src/api/cpp`
2. make a "build/" folder and then `cd` into the folder
3. `cmake -L ..` (**see notes**, this step may need to repeat once for the library, then for examples)
4. `make`
5. `make install` (**optional**)
6. Test with the examples, e.g.,
    1. `examples/_example`
    1. `examples/ogl/tutorial01..`
    1. `examples/kinect/kinect_img`
    1. `examples/kinect/kinect_skeleton`
    1. `examples/libfreenect/glview`
    1. ...

**NOTES:**

Different cmake flags can be enabled, e.g.,
enable OpenCV, but disable examples first:

```
cmake .. -DENABLE_OPENCV:BOOL=ON -DBUILD_EXAMPLES:BOOL=OFF
make
```

Then when libopeniss is built, can add examples, and say
in particular Kinect examples:

```
cmake .. -DENABLE_OPENCV:BOOL=ON -DBUILD_EXAMPLES:BOOL=ON -DENABLE_OPENISS_KINECT_EXAMPLES:BOOL=ON
make
```

Custom provided **FindNuiTrack.cmake, FindOpenNI2.cmake, FindNiTE2.cmake**, etc. rely heavily on environment variables *libNAME_INCLUDE* and *libNAME_REDIST* for include headers and libraries respectively. 

*NOTE: The end goal of these specific FindlibNAME.cmake is to not rely on locally installed libraries, rather platform specific precompiled libraries. However, this is not a final configuration policy and might change over the iterations.*

#### Gesture Framework ####

1. Compile and install libfreenect with OpenNI2 support enabled for Kinect v1.
2. Compile and install libfreenect2 with OpenNI2 support enabled for Kinect v2.
3. Download platform specific OpenNI2
4. Download platform specific NiTE2
5. Export all the mentioned environment variables for OpenNI2, NiTE2 and NuiTrack. 

#### Java and C# wrappers ####

We use SWIG to generate Java wrapper (primarily for Processing) and C# (primarily for Unity). See the `openiss-swig` repo for
details. That repo would link to the `cpp`
folder here to build proper wrapper files.

The SWIG wrapper can be used with minor modifications to generate wrappers for other
languages as well.

#### Windows ####

The preferred way is to use `cmake` or `cmake-gui` to generate proper Visual Studio files and the corresponding solution and use it (it'd be typically found in your `build` folder if you invoke `cmake` from there).

There is also a basic `cpp.vxproj` project / solution provided
to build in Visual Studio 2017. This is currently manually maintained outside of CMake. So when new things
are added, it needs updating, or fixing our CMake build. This build does not account for
examples and only uses a subset of the framework.

#### CMake Options ####

- `BUILD_LIB_ONLY` "Only build the OpenISS library"
- `BUILD_CORE_FRAMEWORK_ONLY` "Only build the OpenISS - abstract core framework (NULL device, etc.)
- `BUILD_EXAMPLES` "Build all the samples"
- `ENABLE_OPENISS_DEBUG` "Enable internal OpenISS debugging"
- `ENABLE_OPENISS_INSTRUMENTATION` "Enable internal OpenISS instrumentation"
- `ENABLE_LIBFREENECT` "Enable libfreenect support"
- `ENABLE_LIBFREENECT2` "Enable libfreenect2 support"
- `ENABLE_REALSENSE` "Enable librealsense2 support" 
- `ENABLE_STRUCTURE` "Enable Structure support" 
- `ENABLE_K4A` "Enable Kinect for Azure support" 
- `ENABLE_OPENNI2` "Enable OpenNI2 support"
- `ENABLE_NITE2` "Enable NiTE2 support"
- `ENABLE_NUITRACK` "Enable NiTE2 support"
- `ENABLE_OPENCV` "Enable OpenCV support"
- `ENABLE_OPENGL` "Enable OpenGL support"
- `ENABLE_ROS` "Enable ROS support"
- `ENABLE_SDL` "Enable SDL support"
- `ENABLE_OFX` "Enable openFrameworks support"
- `ENABLE_OPENFACE` "Enable OpenFace support"
- `ENABLE_YOLO` "Enable YOLO support"
- `ENABLE_PYTHON_ENV` "Enable Python environment 
- `ENABLE_JAVA_ENV` "Enable Java environment support"

## References

- Eric Lai's thesis, [An OpenISS Framework Specialization for Person Re-identification](https://spectrum.library.concordia.ca/985788/)
- Bernie Shen's thesis, [Toward a flexible facial analysis framework in OpenISS for visual effects](https://spectrum.library.concordia.ca/985787/)
- Jashanjot Singh's thesis, [Universal Gesture Tracking Framework in OpenISS and ROS and its Applications](https://spectrum.library.concordia.ca/986429/)
