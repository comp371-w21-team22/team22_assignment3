#include "UserDatum.h"
#include <thread>

// OpenPose dependencies
#include <openpose/headers.hpp>
#include <openpose/core/headers.hpp>
#include <openpose/gui/headers.hpp>
#include <openpose/producer/headers.hpp>
#include <openpose/thread/headers.hpp>
#include <openpose/utilities/headers.hpp>

// GFlags: DEFINE_bool, _int32, _int64, _uint64, _double, _string
#include <gflags/gflags.h>
// Allow Google Flags in Ubuntu 14
#ifndef GFLAGS_GFLAGS_H_
namespace gflags = google;
#endif
DEFINE_int32(logging_level, 3, "The logging level. Integer in the range [0, 255]. 0 will output any log() message, while"
	" 255 will not output any. Current OpenPose library messages are in the range 0-4: 1 for"
	" low priority messages and 4 for important ones.");
// Producer
DEFINE_int32(camera, -1, "The camera index for cv::VideoCapture. Integer in the range [0, 9]. Select a negative"
	" number (by default), to auto-detect and open the first available camera.");
DEFINE_string(camera_resolution, "-1x-1", "Set the camera resolution (either `--camera` or `--flir_camera`). `-1x-1` will use the"
	" default 1280x720 for `--camera`, or the maximum flir camera resolution available for"
	" `--flir_camera`");
DEFINE_double(camera_fps, 30.0, "Frame rate for the webcam (also used when saving video). Set this value to the minimum"
	" value between the OpenPose displayed speed and the webcam real frame rate.");
DEFINE_string(video, "", "Use a video file instead of the camera. Use `examples/media/video.avi` for our default"
	" example video.");
DEFINE_string(image_dir, "", "Process a directory of images. Use `examples/media/` for our default example folder with 20"
	" images. Read all standard formats (jpg, png, bmp, etc.).");
DEFINE_bool(flir_camera, false, "Whether to use FLIR (Point-Grey) stereo camera.");
DEFINE_int32(flir_camera_index, -1, "Select -1 (default) to run on all detected flir cameras at once. Otherwise, select the flir"
	" camera index to run, where 0 corresponds to the detected flir camera with the lowest"
	" serial number, and `n` to the `n`-th lowest serial number camera.");
DEFINE_string(ip_camera, "", "String with the IP camera URL. It supports protocols like RTSP and HTTP.");
DEFINE_bool(process_real_time, false, "Enable to keep the original source frame rate (e.g. for video). If the processing time is"
	" too long, it will skip frames. If it is too fast, it will slow it down.");
DEFINE_string(camera_parameter_folder, "models/cameraParameters/flir/", "String with the folder where the camera parameters are located.");
DEFINE_bool(frame_keep_distortion, false, "If false (default), it will undistortionate the image based on the"
	" `camera_parameter_folder` camera parameters; if true, it will not undistortionate, i.e.,"
	" it will leave it as it is.");
// OpenPose
DEFINE_string(output_resolution, "-1x-1", "The image resolution (display and output). Use \"-1x-1\" to force the program to use the"
	" input image resolution.");
DEFINE_int32(3d_views, 1, "Complementary option to `--image_dir` or `--video`. OpenPose will read as many images per"
	" iteration, allowing tasks such as stereo camera processing (`--3d`). Note that"
	" `--camera_parameters_folder` must be set. OpenPose must find as many `xml` files in the"
	" parameter folder as this number indicates.");
// Consumer
DEFINE_bool(fullscreen, false, "Run in full-screen mode (press f during runtime to toggle).");


UserDatum::UserDatum()
{
}


UserDatum::~UserDatum()
{
}

int load()
{
	// outputSize
	const auto outputSize = op::flagsToPoint(FLAGS_output_resolution, "-1x-1");

	// producerType
	const auto producerSharedPtr = op::flagsToProducer(FLAGS_image_dir, FLAGS_video, FLAGS_ip_camera, FLAGS_camera,
		FLAGS_flir_camera, FLAGS_camera_resolution, FLAGS_camera_fps,
		FLAGS_camera_parameter_folder, !FLAGS_frame_keep_distortion,
		(unsigned int)FLAGS_3d_views, FLAGS_flir_camera_index);
	const auto displayProducerFpsMode = (FLAGS_process_real_time
		? op::ProducerFpsMode::OriginalFps : op::ProducerFpsMode::RetrievalFps);
	producerSharedPtr->setProducerFpsMode(displayProducerFpsMode);

	// Thread worker && manager
	typedef std::vector<op::Datum> TypedefDatumsNoPtr;
	typedef std::shared_ptr<TypedefDatumsNoPtr> TypedefDatums;
	op::ThreadManager<TypedefDatums> threadManager;

	auto DatumProducer = std::make_shared<op::DatumProducer<TypedefDatumsNoPtr>>(producerSharedPtr);
	auto wDatumProducer = std::make_shared<op::WDatumProducer<TypedefDatums, TypedefDatumsNoPtr>>(DatumProducer);
			
	// GUI (Display)
	auto gui = std::make_shared<op::Gui>(outputSize, FLAGS_fullscreen, threadManager.getIsRunningSharedPtr());
	auto wGui = std::make_shared<op::WGui<TypedefDatums>>(gui);

	// ------------------------- CONFIGURING THREADING -------------------------
	// In this simple multi-thread example, we will do the following:
	// 3 (virtual) queues: 0, 1, 2
	// 1 real queue: 1. The first and last queue ids (in this case 0 and 2) are not actual queues, but the
	// beginning and end of the processing sequence
	// 2 threads: 0, 1
	// wDatumProducer will generate frames (there is no real queue 0) and push them on queue 1
	// wGui will pop frames from queue 1 and process them (there is no real queue 2)
	auto threadId = 0ull;
	auto queueIn = 0ull;
	auto queueOut = 1ull;
	threadManager.add(threadId++, wDatumProducer, queueIn++, queueOut++);       // Thread 0, queues 0 -> 1
	threadManager.add(threadId++, wGui, queueIn++, queueOut++);                 // Thread 1, queues 1 -> 2

	op::log("Starting thread(s)...", op::Priority::High);
	threadManager.exec();

	const auto message = "OpenPose demo successfully finished";
	op::log(message, op::Priority::High);

	// Return successful message
	return 0;
}