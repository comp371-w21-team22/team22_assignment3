//
// Created by Haotao Lai on 2018-09-18.
//

// OpenCV support is optional, for viewing convenience
#ifdef OPENISS_OPENCV_SUPPORT
#include <opencv2/opencv.hpp>
#endif

#include "OpenISS.hpp"

using namespace openiss;

void displaySkeleton();

int main(int argc, char** argv)
{
    displaySkeleton();
    return 0;
}

void displaySkeleton()
{
    const char* win1Name = "OpenISS color image with skeleton";

#ifdef OPENISS_OPENCV_SUPPORT
    cv::namedWindow(win1Name);
#endif

    // create an kinect device, enable all features
    OIDeviceFactory devFactory;

#ifdef OPENISS_NITE2_SUPPORT
    shared_ptr<OIDevice> pDevice = devFactory.create("kinect");
#else
    shared_ptr<OIDevice> pDevice = devFactory.create("null");
#endif

    pDevice->open();
    pDevice->enable();

    // create tracker
    OITrackerFactory trackerFactory;
 
#ifdef OPENISS_NITE2_SUPPORT
    shared_ptr<OISkeletonTracker> tracker = trackerFactory.createTracker("nite", pDevice.get());
    shared_ptr<OITrackerFrame> trackerFrame = trackerFactory.createTrackerFrame("nite");
#else
    shared_ptr<OISkeletonTracker> tracker = trackerFactory.createTracker("null", pDevice.get());
    shared_ptr<OITrackerFrame> trackerFrame = trackerFactory.createTrackerFrame("null");
#endif

    bool shutdown = false;
    while(!shutdown)
    {
        // get the registered color frame
        OIFrame* displayFrame = pDevice->readFrame(COLOR_STREAM);

#ifdef OPENISS_DEBUG
        std::cerr << "trackerFrame = " << trackerFrame << std::endl;
#endif

        tracker->readFrame(trackerFrame.get());

#ifdef OPENISS_DEBUG
        std::cerr << "trackerFrame = " << trackerFrame << std::endl;
#endif

        // obtain skeleton and draw it into the color frame
        std::vector<std::shared_ptr<OIUserData>> users = trackerFrame->getUsers();

#ifdef OPENISS_DEBUG
        std::cerr << "user count = " << users.size() << std::endl;
#endif

        for(const auto &u : users)
        {
            OISkeleton* pSkeleton = u->getSkeleton();

#ifdef OPENISS_DEBUG
            std::cerr << "pSkeleton = " << pSkeleton << std::endl;
#endif

#ifdef OPENISS_OPENCV_SUPPORT
//            pSkeleton->mapWorld2Image(tracker.get());
            pSkeleton->drawToFrame(displayFrame, trackerFrame->getSupportedJointType());
#endif
        }

        // display the frame
#ifdef OPENISS_OPENCV_SUPPORT
        displayFrame->show(win1Name);
        int key = cv::waitKey(1);
        shutdown = shutdown || (key > 0 && ((key & 0xFF) == 27)); // shutdown on esc
#endif

    }
}
