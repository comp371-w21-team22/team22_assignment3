# Building ogl C++ examples inside OpenISS SDK

These examples port `opengl-tutorials` examples to be used with OpenISS.

## cmake ##

Most of these steps need to be done once

1. In the root directory download the original `ogl` code: `git submodule update --init --recursive ogl` (or use the quivalent in yout git GUI app)
1. In `ogl`, do the usual `mkdir build`, `cd build`, `cmake ..`, `make`
1. In this directory either copy the below directories from the above to here in Windows or symlink them in Linux/Mac (make sure to never add these to in the OpenISS repo.):
```
ln -s ../../../../../ogl/external
ln -s ../../../../../ogl/distrib
```
3. After that, follow the usual build steps in `cpp`. Make sure the `ENABLE_OGL_BASED_EXAMPLES` variable in the `examples/CMakeLists.txt` is set to `ON`.
   1. This build can then be used to generate Visual Studio or Xcode project files as per usual.

