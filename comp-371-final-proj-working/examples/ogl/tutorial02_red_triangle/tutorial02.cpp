// Include standard headers
#include <stdio.h>
#include <stdlib.h>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <glfw3.h>
GLFWwindow* window;

// Include GLM
#include <glm/glm.hpp>
using namespace glm;

#include <common/shader.hpp>

// Include OpenISS
#include "OpenISS.hpp"
using namespace openiss;

int main( void )
{
    // create an OpenISS device, enable all features
    OIDeviceFactory devFactory;

#ifdef OPENISS_NITE2_SUPPORT
    shared_ptr<OIDevice> pDevice = devFactory.create("kinect");
#else
    shared_ptr<OIDevice> pDevice = devFactory.create("null");
#endif

    pDevice->open();
    pDevice->enable();

    // create tracker
    OITrackerFactory trackerFactory;
 
#ifdef OPENISS_NITE2_SUPPORT
    shared_ptr<OISkeletonTracker> tracker = trackerFactory.createTracker("nite", pDevice.get());
    shared_ptr<OITrackerFrame> trackerFrame = trackerFactory.createTrackerFrame("nite");
#else
    shared_ptr<OISkeletonTracker> tracker = trackerFactory.createTracker("null", pDevice.get());
    shared_ptr<OITrackerFrame> trackerFrame = trackerFactory.createTrackerFrame("null");
#endif


	// Initialise GLFW
	if( !glfwInit() )
	{
		fprintf( stderr, "Failed to initialize GLFW\n" );
		getchar();
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Open a window and create its OpenGL context
	window = glfwCreateWindow( 1024, 768, "Tutorial 02 - Red triangle (OpenISS)", NULL, NULL);
	if( window == NULL ){
		fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
		getchar();
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		getchar();
		glfwTerminate();
		return -1;
	}

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	// Dark blue background
	glClearColor(0.0f, 1.0f, 0.4f, 0.0f);

	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	// Create and compile our GLSL program from the shaders
	GLuint programID = LoadShaders( "SimpleVertexShader.vertexshader", "SimpleFragmentShader.fragmentshader" );


	static GLfloat g_vertex_buffer_data[] = { 
		-1.0f, -1.0f, 0.0f, // left shoulder
		 1.0f, -1.0f, 0.0f, // right shoulder
		 0.0f,  1.0f, 0.0f, // head
	};

	GLuint vertexbuffer;
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_DYNAMIC_DRAW);

	do{

		// Clear the screen
		glClear( GL_COLOR_BUFFER_BIT );

		// Use our shader
		glUseProgram(programID);

        // get the registered color frame
        OIFrame* displayFrame = pDevice->readFrame(COLOR_STREAM);

#ifdef OPENISS_DEBUG
        std::cerr << "trackerFrame = " << trackerFrame << std::endl;
#endif

        tracker->readFrame(trackerFrame.get());

#ifdef OPENISS_DEBUG
        std::cerr << "trackerFrame = " << trackerFrame << std::endl;
#endif

        // obtain skeleton model real-world coordinates
        std::vector<std::shared_ptr<OIUserData>> users = trackerFrame->getUsers();

#ifdef OPENISS_DEBUG
        std::cerr << "user count = " << users.size() << std::endl;
#endif

        for(const auto &u : users)
        {
            OISkeleton* pSkeleton = u->getSkeleton();

#ifdef OPENISS_DEBUG
            std::cerr << "pSkeleton = " << pSkeleton << std::endl;
#endif

			// Update triangle vertex buffer based on the received coordinates
			// mapped to the coordinates of a triangle

			std::shared_ptr<OISkeletonJoint> l_oHead = pSkeleton->getJointByType(JOINT_HEAD);

			std::shared_ptr<OISkeletonJoint> l_oLeftShoulder = pSkeleton->getJointByType(JOINT_LEFT_SHOULDER);

			std::shared_ptr<OISkeletonJoint> l_oRightShoulder = pSkeleton->getJointByType(JOINT_RIGHT_SHOULDER);

			// Update vertex buffer from skeleton joints and normalize.
			// Normalization assumes hardcoded values of 640/480 frim null tracker.
			// These values may chance or be dynamic with various actual trackers.

			g_vertex_buffer_data[0] = l_oLeftShoulder->x / 640;
			g_vertex_buffer_data[1] = l_oLeftShoulder->y / 480;
			g_vertex_buffer_data[2] = l_oLeftShoulder->z;

			g_vertex_buffer_data[3] = l_oRightShoulder->x / 640;
			g_vertex_buffer_data[4] = l_oRightShoulder->y / 480;
			g_vertex_buffer_data[5] = l_oRightShoulder->z;

			g_vertex_buffer_data[6] = l_oHead->x / 640;
			g_vertex_buffer_data[7] = l_oHead->y / 480;
			g_vertex_buffer_data[8] = l_oHead->z;

			// Update buffer object
			glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(g_vertex_buffer_data), g_vertex_buffer_data);

			// 1rst attribute buffer : vertices
			glEnableVertexAttribArray(0);
			glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
			glVertexAttribPointer(
				0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
				3,                  // size
				GL_FLOAT,           // type
				GL_FALSE,           // normalized?
				0,                  // stride
				(void*)0            // array buffer offset
			);
        }

		// Draw the triangle !
		glDrawArrays(GL_TRIANGLES, 0, 3); // 3 indices starting at 0 -> 1 triangle

		glDisableVertexAttribArray(0);

		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();

	} // Check if the ESC key was pressed or the window was closed
	while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
		   glfwWindowShouldClose(window) == 0 );

	// Cleanup VBO
	glDeleteBuffers(1, &vertexbuffer);
	glDeleteVertexArrays(1, &VertexArrayID);
	glDeleteProgram(programID);

	// Close OpenGL window and terminate GLFW
	glfwTerminate();

	return 0;
}

