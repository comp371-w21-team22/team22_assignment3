// Include standard headers
#include <stdio.h>
#include <stdlib.h>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <glfw3.h>
GLFWwindow* window;

// Include GLM
#include <glm/glm.hpp>
using namespace glm;

// Include OpenISS
#include "OpenISS.hpp"
using namespace openiss;

// Context
OpenISS* g_oOpenISS = nullptr;

// Current device
OIDevice* g_oDevice = nullptr;


int main( void )
{
	// Initialize OpenISS
    g_oOpenISS = new OpenISS(NULL_DEVICE);

    OIStatus l_tRetVal = g_oOpenISS->init();

    if(l_tRetVal != OIStatus::STATUS_OK)
    {
        std::cerr
            << "Failed to init OpenISS..."
            << "(" << static_cast<int>(l_tRetVal) << ")"
            << std::endl;

        return EXIT_FAILURE;
    }

    l_tRetVal = g_oOpenISS->enableDepth();

    // ... check l_tRetVal

    l_tRetVal = g_oOpenISS->enableRGB();

    // ... check l_tRetVal

    std::cout << "OpenISS: enabled depth and RGB streams..." << std::endl;

    g_oDevice = g_oOpenISS->getDevice().get();

    if(g_oDevice == nullptr)
    {
        // ...
        std::cerr << "OpenISS: failed to get device..." << std::endl;
        return EXIT_FAILURE;
    }

    OIFrame* l_opFrame = g_oDevice->readFrame(DEPTH_STREAM);

    if(l_opFrame == nullptr)
    {
        // ...
        std::cerr << "OpenISS: null depth frame..." << std::endl;
    }
    else
    {
        std::cout << "OpenISS: acquired depth frame..." << std::endl;
    }

    l_opFrame = g_oDevice->readFrame(COLOR_STREAM);

    if(l_opFrame == nullptr)
    {
        // ...
        std::cerr << "OpenISS: null color frame..." << std::endl;
    }
    else
    {
        std::cout << "OpenISS: acquired color frame..." << std::endl;
    }

	// Initialise GLFW
	if( !glfwInit() )
	{
		fprintf( stderr, "Failed to initialize GLFW\n" );
		getchar();
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Open a window and create its OpenGL context
	window = glfwCreateWindow( 1024, 768, "Tutorial 01 (OpenISS)", NULL, NULL);
	if( window == NULL ){
		fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
		getchar();
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	// Initialize GLEW
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		getchar();
		glfwTerminate();
		return -1;
	}

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	// Dark blue background
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

	do{
		// Clear the screen. It's not mentioned before Tutorial 02, but it can cause flickering, so it's there nonetheless.
		glClear( GL_COLOR_BUFFER_BIT );

		// ...
		// put you OpenISS query code here, such as readFrame(), etc.
		// ...
		// TODO

		// Draw nothing, see you in tutorial 2 !

		
		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();

	} // Check if the ESC key was pressed or the window was closed
	while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
		   glfwWindowShouldClose(window) == 0 );

	// Close OpenGL window and terminate GLFW
	glfwTerminate();

	// Shutdown OpenISS
	delete g_oOpenISS;

	return 0;
}

