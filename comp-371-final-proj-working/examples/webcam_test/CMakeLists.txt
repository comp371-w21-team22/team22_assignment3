#
# This file is a template for OpenISS project
#
# You need to replace:
#    add_executable(_example ${APPSOURCEFILES})
# "_example":
#    with a meaningful name
# 
# Also add "add_subdirectory(YOUR_SUB_EXAMPLE_DIR_NAME)" to
# the "../examples/CMakeLists.txt"

#aux_source_directory(. APPSOURCEFILES)
#add_executable(_example ${APPSOURCEFILES})

set(CMAKE_INSTALL_PREFIX ${CMAKE_SOURCE_DIR}/dist CACHE PATH ${CMAKE_SOURCE_DIR}/dist FORCE)
set(CMAKE_BUILD_TYPE "Debug")

list(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake)
find_package(OpenGL REQUIRED COMPONENTS OpenGL)

find_package( OpenCV REQUIRED )
include_directories( ${OpenCV_INCLUDE_DIRS} )

find_package(OpenISS REQUIRED)

include(BuildGLEW)
include(BuildGLFW)
include(BuildGLM)

set(EXEC webcam_test)
set(ASSETS assets)

add_executable(${EXEC} main.cpp)

link_directories(${PROJECT_BINARY_DIR}/lib)
target_link_libraries(${EXEC} openiss)

target_include_directories(${EXEC} PRIVATE include)

target_link_libraries(${EXEC} OpenGL::GL glew_s glfw glm)
target_link_libraries( ${EXEC} ${OpenCV_LIBS} )

include(utilities)
set(is_debug 1)

link_sample_basic_libs("webcam_test")

list(APPEND BIN ${EXEC})

install(TARGETS ${BIN} DESTINATION ${CMAKE_INSTALL_PREFIX})
install(DIRECTORY ${ASSETS} DESTINATION ${CMAKE_INSTALL_PREFIX})

# if you need to invoke python from C++ uncomment the following lines
# find_package(PythonInterp)
# find_package(PythonLibs)
# find_package(NumPy)
# link_python_libs("_example")

# EOF
