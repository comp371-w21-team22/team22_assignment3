#include "OpenISS.hpp"
#include "FER3D.hpp"

#include <opencv/cv.hpp>
#include <opencv2/core.hpp>

#include <iostream>

#include <vector>

using namespace openiss;

void testImages() {
    FER3D classifier;

    cv::Mat img1 = cv::imread("/home/gipsy/Documents/yiran/openiss/src/api/cpp/examples/fer3d/test/1.png");
    cv::Mat img2 = cv::imread("/home/gipsy/Documents/yiran/openiss/src/api/cpp/examples/fer3d/test/2.png");
    cv::Mat img3 = cv::imread("/home/gipsy/Documents/yiran/openiss/src/api/cpp/examples/fer3d/test/3.png");
    cv::Mat img4 = cv::imread("/home/gipsy/Documents/yiran/openiss/src/api/cpp/examples/fer3d/test/4.png");
    cv::Mat img5 = cv::imread("/home/gipsy/Documents/yiran/openiss/src/api/cpp/examples/fer3d/test/5.png");
    cv::Mat img6 = cv::imread("/home/gipsy/Documents/yiran/openiss/src/api/cpp/examples/fer3d/test/6.png");
    cv::Mat img7 = cv::imread("/home/gipsy/Documents/yiran/openiss/src/api/cpp/examples/fer3d/test/7.png");
    cv::Mat img8 = cv::imread("/home/gipsy/Documents/yiran/openiss/src/api/cpp/examples/fer3d/test/8.png");

    std::vector<cv::Mat> imageVector;
    imageVector.push_back(img1);
    imageVector.push_back(img2);
    imageVector.push_back(img3);
    imageVector.push_back(img4);
    imageVector.push_back(img5);
    imageVector.push_back(img6);
    imageVector.push_back(img7);
    imageVector.push_back(img8);

    cv::Mat result = classifier.predict(imageVector);
    std::cout << "predict result :" << std::endl;
    std::cout << result << std::endl;

}

void testKinectFlowWithFER3D(bool mode) {

    int FRAME_NUM = 4;

    OIDevFactory factory;
    std::shared_ptr<OIDevice> pDevice = factory.create("kinect");
    pDevice->open();
    pDevice->enableColor();

    OIFaceAdapterFactory adapterFactory;
    std::shared_ptr<OIFaceAdapter> faceAdapter = adapterFactory.createAdapter("openface", pDevice.get());

    FER3D classifier;


    int cc = 1;
    bool shutdown = false;

    // create a vector<vector<Mat>> to record different face vector -> face sequence vector
    std::vector<std::vector<cv::Mat>> allFaceVector;

    // using vector<Mat> to record predict expression, and calculate change percentage of expression to represent expression
    std::vector<cv::Mat> facePredictVector;
    std::string strResult = "";
    while (!shutdown) {
        OIFrame* colorFrame = pDevice->readFrame(openiss::COLOR_STREAM);

        cv::Mat colorImg = colorFrame->getOpencvMat();

        OIFace oiFace(colorImg);

        faceAdapter->initializeFace(oiFace);

        //make sure allFaceVector has less or equal face number with faceVector
        while (oiFace.faceVector.size() > allFaceVector.size()) {
            std::vector<cv::Mat> tempSequence;
            allFaceVector.push_back(tempSequence);
        }

        for (int i = 0; i < oiFace.faceVector.size(); i++) {

            /*
             * draw facial landmarks on face
             */
            // std::vector<cv::Point2f> points = oiFace.facialLandmarksVector.at(i);
            // for (int j = 0; j < points.size(); j++)
            // {
            //     cv::circle(oiFace.faceVector[i], points.at(j), 2, cv::Scalar(0, 0, 250));
            // }
            cv::imshow("face_detect+landmarks"+std::to_string(i), oiFace.faceVector[i]);

            /*
             * resize and save face to faceVector
             */
            cv::Mat outImg;
            cv::resize(oiFace.faceVector[i], outImg, cv::Size(64, 64), 0, 0, CV_INTER_LINEAR);
            //convert colorImg to 3 channel gray image
            Mat gray;
            cv::cvtColor(outImg, gray, CV_BGR2GRAY);
            cv::cvtColor(gray, outImg, CV_GRAY2RGB);
            //update allFaceVector[i] sequence, make sure it always save latest FRAME_NUM sequence
            if (allFaceVector[i].size() < FRAME_NUM) {
                allFaceVector[i].push_back(outImg);
            } else if (allFaceVector[i].size() == FRAME_NUM) {

                cv::Mat result = classifier.predict(allFaceVector[i]);

                // initial facePredictVector
                while (facePredictVector.size() < oiFace.faceVector.size()) {
                    facePredictVector.push_back(result);
                }

                // //compare fer result with last result
                // float maxDiff = -1;     // using % to compare
                // int maxDiffExpression = -1;
                // for (int k = 0; k < result.cols; k++) {
                //     //if kth expression has largest probability increase that's the expression we want
                //     float newProb = result.at<float>(0, k);
                //     float oldProb = facePredictVector[i].at<float>(0, k);
                //     if (newProb > oldProb) {
                //         float diffProb = (newProb - oldProb)/oldProb;
                //         if (diffProb > maxDiff && diffProb > 100) {
                //             maxDiff = diffProb;
                //             maxDiffExpression = k;
                //         }
                //     }
                // }

                // // print predict result and expression predict result
                strResult = "";
                // std::cout << result << std::endl;
                // if (maxDiffExpression == -1) {
                //     std::cout << "Expression for :"<< i <<" is natural: " << maxDiff << std::endl;
                //     strResult = "Expression for :"+ to_string(i) +" is natural: " + to_string(maxDiff);
                // }
                // if (maxDiffExpression == 0) {
                //     std::cout << "Expression for :"<< i <<" is angry: " << maxDiff << std::endl;
                //     strResult = "Expression for :"+ to_string(i) +" is angry: " + to_string(maxDiff);
                // }
                // if (maxDiffExpression == 1) {
                //     std::cout << "Expression for :"<< i <<" is disgusted: " << maxDiff << std::endl;
                //     strResult = "Expression for :"+ to_string(i) +" is disgusted: " + to_string(maxDiff);
                // }
                // if (maxDiffExpression == 2) {
                //     std::cout << "Expression for :"<< i <<" is fearful: " << maxDiff << std::endl;
                //     strResult = "Expression for :"+ to_string(i) +" is fearful: " + to_string(maxDiff);
                // }
                // if (maxDiffExpression == 3) {
                //     std::cout << "Expression for :"<< i <<" is happy: " << maxDiff << std::endl;
                //     strResult = "Expression for :"+ to_string(i) +" is happy: " + to_string(maxDiff);
                // }
                // if (maxDiffExpression == 4) {
                //     std::cout << "Expression for :"<< i <<" is sadness: " << maxDiff << std::endl;
                //     strResult = "Expression for :"+ to_string(i) +" is sadness: " + to_string(maxDiff);
                // }
                // if (maxDiffExpression == 5) {
                //     std::cout << "Expression for :"<< i <<" is surprised: " << maxDiff << std::endl;
                //     strResult = "Expression for :"+ to_string(i) +" is surprised: " + to_string(maxDiff);
                // }

                // stop when we detect the face expression
                if (mode == true) {
                    for (int m = 0; m < allFaceVector[i].size(); m++) {
                        cv::imshow(to_string(m), allFaceVector[i][m]);
                    }
                    cv::waitKey(0);
                }
                

                // replace facePredictVector with new result
                facePredictVector[i] = result;

                // clear out whole vector
                allFaceVector[i].clear();
            } else {
                allFaceVector[i].clear();
            }
        }

        cv::putText(colorImg, strResult, cv::Point(30,30), cv::FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(0,0,250), 1.5);
        cv::imshow("color image", oiFace.originalImg);

        int key = cv::waitKey(1);
        // shutdown on esc
        shutdown = shutdown || (key > 0 && ((key & 0xFF) == 27));
        // save image
        cc++;
        
        switch(key){

            case 's':
                break;
            case 'q':
                return ;
                break;
            default:
                break;
        }
    }
}

void test() {
    cv::Mat img = cv::imread("/home/gipsy/Documents/yiran/openiss/src/api/cpp/examples/fer3d/test/6.png");
    std::cout <<"asdf" << std::endl;
    cv::imshow("asdf", img);
    cv::waitKey(0);
}
int main() {
    // testImages();
    testKinectFlowWithFER3D(true);
    // test();
    return 0;
}