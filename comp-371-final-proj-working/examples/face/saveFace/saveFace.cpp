//
// Created by Haotao Lai on 2018-08-08.
//

#include <opencv/cv.hpp>
#include <opencv2/core.hpp>

#include "OpenISS.hpp"

#include <cstring>
#include <iostream>


using namespace openiss;

void saveFace();

int main() {
    saveFace();
    return 0;
}

/**
 * The following method shows how to use OpenISS API
 * to get RGB and depth image data
 */
void saveFace() {
    const char* win1Name = "color image";

    cv::namedWindow(win1Name);

    OIDevFactory factory;
    std::shared_ptr<OIDevice> pDevice = factory.create("kinect");
    pDevice->open();
    pDevice->enableColor();

    OIFaceAdapterFactory adapterFactory;
    std::shared_ptr<OIFaceAdapter> faceAdapter = adapterFactory.createAdapter("openface", pDevice.get());


    int cc = 1;
    bool shutdown = false;
    while (!shutdown) {
        OIFrame* colorFrame = pDevice->readFrame(openiss::COLOR_STREAM);

        cv::Mat img = colorFrame->getOpencvMat();

        OIFace oiFace(img);

        faceAdapter->initializeFace(oiFace);

        for (int i = 0; i < oiFace.faceVector.size(); i++) {


            // draw facial landmarks on face
            // std::vector<cv::Point2f> points = oiFace.facialLandmarksVector.at(i);
            // for (int j = 0; j < points.size(); j++)
            // {
            //     cv::circle(oiFace.faceVector[i], points.at(j), 2, cv::Scalar(0, 0, 250));
            // }

            cv::imshow("face_detect+landmarks"+std::to_string(i), oiFace.faceVector[i]);

            //resize and save face to iss_face_images
            cv::Mat outImg;
            cv::resize(oiFace.faceVector[i], outImg, cv::Size(64, 64), 0, 0, CV_INTER_LINEAR);
            cv::imwrite("/home/gipsy/Desktop/iss_face_images/"+std::to_string(cc++)+".png", outImg);
            std::cout << "rows :" <<outImg.rows <<std::endl;
            std::cout << "cols :" <<outImg.cols <<std::endl;

        }


        colorFrame->show(win1Name);

        int key = cv::waitKey(1);
        // shutdown on esc
        shutdown = shutdown || (key > 0 && ((key & 0xFF) == 27));
        // save image
        cc++;
        
        switch(key){
            case 's':
                break;
            default:
                break;
        }
    }
}
