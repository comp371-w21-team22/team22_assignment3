//
// Created by Haotao Lai on 2018-08-08.
//

#include <opencv/cv.hpp>

#include "penISS.hpp"

using namespace openiss;

void displayColorAndDepth();

int main() {
    displayColorAndDepth();
    return 0;
}

/**
 * The following method shows how to use OpenISS API
 * to get RGB and depth image data
 */
void displayColorAndDepth() {
    const char* win1Name = "Single Face Landmarks";

    OIDevFactory factory;
    std::shared_ptr<OIDevice> pDevice = factory.create("kinect");
    pDevice->open();
    pDevice->enableColor();
    pDevice->enableDepth();

    OIFaceAdapterFactory adapterFactory;
    std::shared_ptr<OIFaceAdapter> faceAdapter = adapterFactory.createAdapter("openface", pDevice.get());

    bool shutdown = false;
    while (!shutdown) {
        OIFrame* colorFrame = pDevice->readFrame(openiss::COLOR_STREAM);

        cv::Mat img = colorFrame->getOpencvMat();

        faceAdapter->drawMultiFacesOnImg(img);
        
        
        OIFrame* depthFrame = pDevice->readFrame(openiss::DEPTH_STREAM);
        cv::imshow("depth", (depthFrame->getOpencvMat()));
        int key = cv::waitKey(1);
        // shutdown on esc
        shutdown = shutdown || (key > 0 && ((key & 0xFF) == 27));
        
        switch(key){
            case 's':
                break;
            default:
                break;
        }

    }
}
