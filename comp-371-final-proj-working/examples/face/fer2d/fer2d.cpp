#include "OpenISS.hpp"
#include "FER2D.hpp"

#include <opencv/cv.hpp>
#include <opencv2/core.hpp>

#include <iostream>
#include <ctime>
#include <vector>

using namespace openiss;

void testImages() {
    FER2D classifier;

    cv::Mat img1 = cv::imread("/home/gipsy/Documents/yiran/openiss/src/api/cpp/examples/fer2d/test/1.png");

    cv::Mat result = classifier.predict(img1);
    std::cout << "predict result :" << std::endl;
    std::cout << result << std::endl;

}

void testKinectFlowWithFER2D(bool mode) {

    OIDevFactory factory;
    std::shared_ptr<OIDevice> pDevice = factory.create("kinect");
    pDevice->open();
    pDevice->enableColor();

    OIFaceAdapterFactory adapterFactory;
    std::shared_ptr<OIFaceAdapter> faceAdapter = adapterFactory.createAdapter("openface", pDevice.get());

    FER2D classifier;


    int cc = 1;
    bool shutdown = false;

    std::string strResult = "";
    while (!shutdown) {
        OIFrame* colorFrame = pDevice->readFrame(openiss::COLOR_STREAM);

        cv::Mat colorImg = colorFrame->getOpencvMat();

        OIFace oiFace(colorImg);

        faceAdapter->initializeFace(oiFace);

        for (int i = 0; i < oiFace.faceVector.size(); i++) {

            /*
             * draw facial landmarks on face
             */
            // std::vector<cv::Point2f> points = oiFace.facialLandmarksVector.at(i);
            // for (int j = 0; j < points.size(); j++)
            // {
            //     cv::circle(oiFace.faceVector[i], points.at(j), 2, cv::Scalar(0, 0, 250));
            // }
            cv::imshow("face_detect+landmarks"+std::to_string(i), oiFace.faceVector[i]);

            /*
             * resize and save face to faceVector
             */
            cv::Mat outImg;
            cv::resize(oiFace.faceVector[i], outImg, cv::Size(48, 48), 0, 0, CV_INTER_LINEAR);

            // get result from classifier and save in strResult
            cv::Mat result = classifier.predict(outImg);
            strResult = "";

            for (int k = 0; k < result.cols; k++) {
                if (k == 0) {
                    strResult = strResult + "angry: " + to_string(result.at<float>(0, k)) + "\n";
                }
                if (k == 1) {
                    strResult = strResult + "disgusted: " + to_string(result.at<float>(0, k)) + "\n";
                }
                if (k == 2) {
                    strResult = strResult + "fearful: " + to_string(result.at<float>(0, k)) + "\n";
                }
                if (k == 3) {
                    strResult = strResult + "happy: " + to_string(result.at<float>(0, k)) + "\n";
                }
                if (k == 4) {
                    strResult = strResult + "sadness: " + to_string(result.at<float>(0, k)) + "\n";
                }
                if (k == 5) {
                    strResult = strResult + "surprised: " + to_string(result.at<float>(0, k)) + "\n";
                }
                if (k == 6) {
                    strResult = strResult + "neutral: " + to_string(result.at<float>(0, k)) + "\n";
                }
            }
            

        }

        // cv::putText(colorImg, strResult, cv::Point(10,10), cv::FONT_HERSHEY_COMPLEX_SMALL, 0.5, cvScalar(0,0,250), 1.5);
        std::cout << strResult << std::endl;

        cv::imshow("color image", oiFace.originalImg);

        int key = cv::waitKey(1);
        if (mode == true) {
            cv::waitKey(0);
        }

        // shutdown on esc
        shutdown = shutdown || (key > 0 && ((key & 0xFF) == 27));
        // save image
        cc++;
        
        switch(key){

            case 's':
                break;
            case 'q':
                return ;
                break;
            default:
                break;
        }
    }
}

void testOIOpenFaceAdapter(bool mode) {

    OIDevFactory factory;
    std::shared_ptr<OIDevice> pDevice = factory.create("kinect");
    pDevice->open();
    pDevice->enableColor();

    OIFaceAdapterFactory adapterFactory;
    std::shared_ptr<OIFaceAdapter> faceAdapter = adapterFactory.createAdapter("openface", pDevice.get());

    int cc = 1;
    bool shutdown = false;
    int image_count = 0;
    std::time_t startTime;
    std::time_t endTime;

    std::string strResult = "";
    while (!shutdown) {

        // calculate time to cost per 100 image
        if (image_count == 0) {
            startTime = std::time(NULL);
        }
        if (image_count == 100) {
            endTime = std::time(NULL);
            std::cout << "Time cost for per 100 images is :" << endTime - startTime << std::endl;
            image_count = -1;
        }
        image_count++;

        OIFrame* colorFrame = pDevice->readFrame(openiss::COLOR_STREAM);

        cv::Mat colorImg = colorFrame->getOpencvMat();

        OIFace oiFace(colorImg);

        faceAdapter->initializeFace(oiFace);

        /*
         *  draw detect face and draw facial landmarks
         */ 
        for (int i = 0; i < oiFace.faceVector.size(); i++) {

            // /*
            //  * draw facial landmarks on face
            //  */
            // std::vector<cv::Point2f> points = oiFace.facialLandmarksVector.at(i);
            // for (int j = 0; j < points.size(); j++)
            // {
            //     cv::circle(oiFace.faceVector[i], points.at(j), 2, cv::Scalar(0, 0, 250));
            // }
            cv::imshow("face_detect+landmarks"+std::to_string(i), oiFace.faceVector[i]);
        }

        /*
         * print expression detect result
         */
        for (int i = 0; i < oiFace.expressionVector.size(); i++) {
            // get result from classifier and save in strResult
            cv::Mat result = oiFace.expressionVector[i];
            strResult = "";

            for (int k = 0; k < result.cols; k++) {
                if (k == 0) {
                    strResult = strResult + "angry: " + to_string(result.at<float>(0, k)) + "\n";
                }
                if (k == 1) {
                    strResult = strResult + "disgusted: " + to_string(result.at<float>(0, k)) + "\n";
                }
                if (k == 2) {
                    strResult = strResult + "fearful: " + to_string(result.at<float>(0, k)) + "\n";
                }
                if (k == 3) {
                    strResult = strResult + "happy: " + to_string(result.at<float>(0, k)) + "\n";
                }
                if (k == 4) {
                    strResult = strResult + "sadness: " + to_string(result.at<float>(0, k)) + "\n";
                }
                if (k == 5) {
                    strResult = strResult + "surprised: " + to_string(result.at<float>(0, k)) + "\n";
                }
                if (k == 6) {
                    strResult = strResult + "neutral: " + to_string(result.at<float>(0, k)) + "\n";
                }
            }

            // cv::putText(colorImg, strResult, cv::Point(10,10), cv::FONT_HERSHEY_COMPLEX_SMALL, 0.5, cvScalar(0,0,250), 1.5);
            std::cout << "Person: " << i << std::endl;
            std::cout << strResult << std::endl;
        }



        cv::imshow("color image", oiFace.originalImg);

        int key = cv::waitKey(1);
        if (mode == true) {
            cv::waitKey(0);
        }

        // shutdown on esc
        shutdown = shutdown || (key > 0 && ((key & 0xFF) == 27));
        // save image
        cc++;
        
        switch(key){

            case 's':
                break;
            case 'q':
                return ;
                break;
            default:
                break;
        }
    }
}

int main() {
    // testImages();
    // testKinectFlowWithFER2D(true);
    testOIOpenFaceAdapter(false);

    return 0;
}