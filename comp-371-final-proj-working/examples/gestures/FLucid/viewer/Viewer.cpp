#include "Scene.hpp"
#include <fstream>
#include <cstdlib>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include "imgui/imgui.h"
#include "imgui/imgui_impl_sdl.h"
#include "imgui/imgui_impl_opengl3.h"
#include <algorithm>

// global variables
int WINDOW_WIDTH = 640;
int WINDOW_HEIGHT = 480;
constexpr int SPHERE_COUNT = 4;
//constexpr int RANDOM_SPHERE_COUNT = 4;
constexpr float SPHERE_SCALE_FACTOR = 180.0f;
constexpr float HANDPTR_SCALE_FACTOR = 60.0f;
#define MESH_FILE "sphere.obj"
std::string output_data;
std::vector<int> sequence;
std::string sequence_string_a;
std::string sequence_string_b;

//*********************************************************************************
// UTILITY CODE
//*********************************************************************************

inline bool FileExists(const std::string& path)
{
    std::ifstream file(path.c_str());
    return file.good();
}

bool SaveToFile(const std::string& path, const std::string& data)
{
    if(FileExists(path)) {
        std::ofstream file(path);
        file << data;
    } else {
        std::ofstream file(path);
        file << data;
    }
    return true;
}

//*********************************************************************************
// OPENGL UTILITY CODE
//*********************************************************************************

const char * GetGLErrorStr(GLenum err)
{
    switch (err)
    {
        case GL_NO_ERROR:          return "No error";
        case GL_INVALID_ENUM:      return "Invalid enum";
        case GL_INVALID_VALUE:     return "Invalid value";
        case GL_INVALID_OPERATION: return "Invalid operation";
        case GL_STACK_OVERFLOW:    return "Stack overflow";
        case GL_STACK_UNDERFLOW:   return "Stack underflow";
        case GL_OUT_OF_MEMORY:     return "Out of memory";
        default:                   return "Unknown error";
    }
}

void CheckGLError()
{
    while (true)
    {
        const GLenum err = glGetError();
        if (GL_NO_ERROR == err)
            break;
        std::cout << "GL Error: " << GetGLErrorStr(err) << std::endl;
    }
}

bool ParseFileToStr(const char *file_name, char *shader_str, int max_len) {
    FILE *file = fopen( file_name, "r" );
    if ( !file ) {
        printf("ERROR: opening file for reading: %s\n", file_name);
        return false;
    }
    size_t cnt = fread( shader_str, 1, max_len - 1, file );
    if ( (int)cnt >= max_len - 1 ) {
        printf("WARNING: file %s too big - truncated.\n", file_name);
    }
    if ( ferror( file ) ) {
        printf("ERROR: reading shader file %s\n", file_name);
        fclose( file );
        return false;
    }
    // append \0 to end of file string
    shader_str[cnt] = 0;
    fclose( file );
    return true;
}

void ShaderLog(GLuint shader_index)
{
    int max_length = 2048;
    int actual_length = 0;
    char log[2048];
    glGetShaderInfoLog(shader_index, max_length, &actual_length, log);
    printf("shader info log for GL index %i:\n%s\n", shader_index, log);
}

// Only for square matrices
//void PrintMatrix(glm::mat4 matrix, int size = 4)
//{
//    for(int i = 0; i < size; ++i) {
//        for (int j = 0; j < size; ++j) {
//            std::cout << matrix[i][j] << " ";
//        }
//        std::cout << std::endl;
//    }
//}

// loads sphere from sphere.obj
bool LoadObjFile( const char *file_name, float *&points, float *&tex_coords,
                  float *&normals, int &point_count )
{

    float *unsorted_vp_array = NULL;
    float *unsorted_vt_array = NULL;
    float *unsorted_vn_array = NULL;
    int current_unsorted_vp = 0;
    int current_unsorted_vt = 0;
    int current_unsorted_vn = 0;

    FILE *fp = fopen( file_name, "r" );
    if ( !fp ) {
        fprintf( stderr, "ERROR: could not find file %s\n", file_name );
        return false;
    }

    // first count points in file so we know how much mem to allocate
    point_count = 0;
    int unsorted_vp_count = 0;
    int unsorted_vt_count = 0;
    int unsorted_vn_count = 0;
    int face_count = 0;
    char line[1024];
    while ( fgets( line, 1024, fp ) ) {
        if ( line[0] == 'v' ) {
            if ( line[1] == ' ' ) {
                unsorted_vp_count++;
            } else if ( line[1] == 't' ) {
                unsorted_vt_count++;
            } else if ( line[1] == 'n' ) {
                unsorted_vn_count++;
            }
        } else if ( line[0] == 'f' ) {
            face_count++;
        }
    }
    printf( "found %i vp %i vt %i vn unique in obj. allocating memory...\n",
            unsorted_vp_count, unsorted_vt_count, unsorted_vn_count );
    unsorted_vp_array = (float *)malloc( unsorted_vp_count * 3 * sizeof( float ) );
    unsorted_vt_array = (float *)malloc( unsorted_vt_count * 2 * sizeof( float ) );
    unsorted_vn_array = (float *)malloc( unsorted_vn_count * 3 * sizeof( float ) );
    points = (float *)malloc( 3 * face_count * 3 * sizeof( float ) );
    tex_coords = (float *)malloc( 3 * face_count * 2 * sizeof( float ) );
    normals = (float *)malloc( 3 * face_count * 3 * sizeof( float ) );
    printf( "allocated %i bytes for mesh\n",
            (int)( 3 * face_count * 8 * sizeof( float ) ) );

    rewind( fp );
    while ( fgets( line, 1024, fp ) ) {
        // vertex
        if ( line[0] == 'v' ) {

            // vertex point
            if ( line[1] == ' ' ) {
                float x, y, z;
                x = y = z = 0.0f;
                sscanf( line, "v %f %f %f", &x, &y, &z );
                unsorted_vp_array[current_unsorted_vp * 3] = x;
                unsorted_vp_array[current_unsorted_vp * 3 + 1] = y;
                unsorted_vp_array[current_unsorted_vp * 3 + 2] = z;
                current_unsorted_vp++;

                // vertex texture coordinate
            } else if ( line[1] == 't' ) {
                float s, t;
                s = t = 0.0f;
                sscanf( line, "vt %f %f", &s, &t );
                unsorted_vt_array[current_unsorted_vt * 2] = s;
                unsorted_vt_array[current_unsorted_vt * 2 + 1] = t;
                current_unsorted_vt++;

                // vertex normal
            } else if ( line[1] == 'n' ) {
                float x, y, z;
                x = y = z = 0.0f;
                sscanf( line, "vn %f %f %f", &x, &y, &z );
                unsorted_vn_array[current_unsorted_vn * 3] = x;
                unsorted_vn_array[current_unsorted_vn * 3 + 1] = y;
                unsorted_vn_array[current_unsorted_vn * 3 + 2] = z;
                current_unsorted_vn++;
            }

            // faces
        } else if ( line[0] == 'f' ) {
            // work out if using quads instead of triangles and print a warning
            int slashCount = 0;
            int len = strlen( line );
            for ( int i = 0; i < len; i++ ) {
                if ( line[i] == '/' ) {
                    slashCount++;
                }
            }
            if ( slashCount != 6 ) {
                fprintf( stderr,
                         "ERROR: file contains quads or does not match v vp/vt/vn layout - \
                    make sure exported mesh is triangulated and contains vertex points, \
                    texture coordinates, and normals\n" );
                return false;
            }

            int vp[3], vt[3], vn[3];
            sscanf( line, "f %i/%i/%i %i/%i/%i %i/%i/%i", &vp[0], &vt[0], &vn[0], &vp[1],
                    &vt[1], &vn[1], &vp[2], &vt[2], &vn[2] );

            /* start reading points into a buffer. order is -1 because obj starts from
                             1, not 0 */
            // NB: assuming all indices are valid
            for ( int i = 0; i < 3; i++ ) {
                if ( ( vp[i] - 1 < 0 ) || ( vp[i] - 1 >= unsorted_vp_count ) ) {
                    fprintf( stderr, "ERROR: invalid vertex position index in face\n" );
                    return false;
                }
                if ( ( vt[i] - 1 < 0 ) || ( vt[i] - 1 >= unsorted_vt_count ) ) {
                    fprintf( stderr, "ERROR: invalid texture coord index %i in face.\n",
                             vt[i] );
                    return false;
                }
                if ( ( vn[i] - 1 < 0 ) || ( vn[i] - 1 >= unsorted_vn_count ) ) {
                    printf( "ERROR: invalid vertex normal index in face\n" );
                    return false;
                }
                points[point_count * 3] = unsorted_vp_array[( vp[i] - 1 ) * 3];
                points[point_count * 3 + 1] = unsorted_vp_array[( vp[i] - 1 ) * 3 + 1];
                points[point_count * 3 + 2] = unsorted_vp_array[( vp[i] - 1 ) * 3 + 2];
                tex_coords[point_count * 2] = unsorted_vt_array[( vt[i] - 1 ) * 2];
                tex_coords[point_count * 2 + 1] = unsorted_vt_array[( vt[i] - 1 ) * 2 + 1];
                normals[point_count * 3] = unsorted_vn_array[( vn[i] - 1 ) * 3];
                normals[point_count * 3 + 1] = unsorted_vn_array[( vn[i] - 1 ) * 3 + 1];
                normals[point_count * 3 + 2] = unsorted_vn_array[( vn[i] - 1 ) * 3 + 2];
                point_count++;
            }
        }
    }
    fclose( fp );
    free( unsorted_vp_array );
    free( unsorted_vn_array );
    free( unsorted_vt_array );
    printf( "allocated %i points\n", point_count );
    return true;
}

//*********************************************************************************
// Camera
//*********************************************************************************

Camera::Camera(glm::vec3 position, glm::vec3 direction, glm::vec3 up) :
        h_fov(HFOV),
        v_fov(VFOV),
        near_plane(NEAR_PLANE),
        far_plane(FAR_PLANE),
        viewport_aspect_ratio(WINDOW_WIDTH/WINDOW_HEIGHT),
        camera_speed(CAMERA_SPEED)
{
    this->position = position;
    this->direction = direction;
    this->up_vector = up;
}

Camera::Camera(float posX, float posY, float posZ, float directionX, float directionY, float directionZ, float upX, float upY, float upZ) :
        h_fov(HFOV),
        v_fov(VFOV),
        near_plane(FAR_PLANE),
        far_plane(FAR_PLANE),
        viewport_aspect_ratio(WINDOW_WIDTH / WINDOW_HEIGHT),
        camera_speed(CAMERA_SPEED)
{
    this->position = glm::vec3(posX, posY, posZ);
    this->up_vector = glm::vec3(upX, upY, upZ);
}

glm::vec3 Camera::GetPosition() const { return this->position; }
void Camera::SetPosition(const glm::vec3& position) { this->position = position; }
void Camera::OffsetPosition(const glm::vec3& offset_value) { this->position += offset_value; }

glm::vec3 Camera::GetDirection() const { return this->direction; }
void Camera::SetDirection(const glm::vec3& direction) { this->direction = direction; }

float Camera::GetHorizontalFieldOfView() const { return this->h_fov; }
void Camera::SetHorizontalFieldOfView(float h_fov) { this->h_fov = h_fov; }
float Camera::GetVerticalFieldOfView() const { return this->v_fov; }
void Camera::SetVerticalFieldOfView(float v_fov) { this->v_fov = v_fov; }

float Camera::GetNearPlane() const { return this->near_plane; }
void Camera::SetNearPlane(float near_plane) {}
float Camera::GetFarPlane() const { return this->far_plane; }
void Camera::SetFarPlane(float far_plane) {}
void Camera::SetNearFarPlanes(float near_plane, float far_plane) {}

float Camera::GetViewportAspectRatio() const { return this->viewport_aspect_ratio; }
void Camera::SetViewportAspectRatio(float viewport_aspect_ratio) {}

float Camera::GetCameraSpeed() const { return this->camera_speed; }
void Camera::SetCameraSpeed(float speed) { this->camera_speed = speed; }

// TODO: Fix camera motion and add mouse control
void Camera::UpdateLookAt()
{
    const Uint8* keystate = SDL_GetKeyboardState(nullptr);
    if (keystate[SDL_SCANCODE_W] || keystate[SDL_SCANCODE_UP]) {
        // glm::vec3 camera_position = glm::vec3(0.0f, 0.0f, 1.0f);
        // glm::vec3 direction = glm::normalize(camera_position - (camera_position + glm::vec3(0.0f, 0.0f, -1.0f)));
        glm::vec3 direction = glm::vec3(0.0f, 0.0f, 1.0f);
        this->position += camera_speed * direction;
        this->direction += camera_speed * direction;
    }

    if (keystate[SDL_SCANCODE_S] || keystate[SDL_SCANCODE_DOWN]) {
        glm::vec3 direction = glm::vec3(0.0f, 0.0f, -1.0f);
        this->position += camera_speed * direction;
        this->direction += camera_speed * direction;
    }

    if (keystate[SDL_SCANCODE_D] || keystate[SDL_SCANCODE_RIGHT]) {
        glm::vec3 direction = glm::vec3(-1.0f, 0.0f, 0.0f);
        this->position += camera_speed * direction;
        this->direction += camera_speed * direction;
    }

    if (keystate[SDL_SCANCODE_A] || keystate[SDL_SCANCODE_LEFT]) {
        glm::vec3 direction = glm::vec3(1.0f, 0.0f, 0.0f);
        this->position += camera_speed * direction;
        this->direction += camera_speed * direction;
    }
}

glm::mat4 Camera::GetViewMatrix() const { return glm::lookAt(this->position, this->direction, this->up_vector); }

//*********************************************************************************
// GAMERENDERER CODE
//*********************************************************************************

GameRenderer::GameRenderer()
{
    make_sphere_stationary = false;
    if_sphere_selected = false;
#ifdef OPENISS_NITE_SUPPORT
    tracker = new openiss::OINiTEGestureTracker();
#elif OPENISS_NUITRACK_SUPPORT
    tracker = new openiss::OINuiTrackGestureTracker();
#else
    tracker = new openiss::OINullGestureTracker();
#endif

    tracker->init();
    tracker->startGestureDetection();
}

void GameRenderer::SetupShaders(const char* vshader_fname, const char* fshader_fname, GLuint* shader_program)
{
    char vertex_shader[1024 * 256];
    char fragment_shader[1024 * 256];
    ParseFileToStr(vshader_fname, vertex_shader, 1024 * 256);
    ParseFileToStr(fshader_fname, fragment_shader, 1024 * 256);

    GLuint vs = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* p = (const GLchar *)vertex_shader;
    glShaderSource(vs, 1, &p, NULL);
    glCompileShader(vs);
    int params = -1;
    glGetShaderiv( vs, GL_COMPILE_STATUS, &params );
    if ( GL_TRUE != params ) {
        ShaderLog(vs);
        exit(EXIT_FAILURE);
    }
    GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
    p = (const GLchar *)fragment_shader;
    glShaderSource(fs, 1, &p, NULL);
    glCompileShader(fs);
    params = -1;
    glGetShaderiv( fs, GL_COMPILE_STATUS, &params );
    if ( GL_TRUE != params ) {
        ShaderLog(fs);
        exit(EXIT_FAILURE);
    }
    *shader_program = glCreateProgram();
    glAttachShader(*shader_program, vs);
    glAttachShader(*shader_program, fs);
    glLinkProgram(*shader_program);
}

bool GameRenderer::LoadTexture(const char* filename, GLuint* tex)
{
    int x, y, n;
    int force_channels = 4;
    // the following function call flips the image
    // needs to be called before each stbi_load(...);
    stbi_set_flip_vertically_on_load(true);
    unsigned char *image_data = stbi_load( filename, &x, &y, &n, force_channels );
    if ( !image_data ) {
        fprintf( stderr, "ERROR: could not load %s\n", filename );
        return false;
    } else {
        printf("Image Loaded\n");
    }
    // NPOT check
    if ( ( x & ( x - 1 ) ) != 0 || ( y & ( y - 1 ) ) != 0 ) {
        fprintf( stderr, "WARNING: texture %s is not power-of-2 dimensions\n",
                 filename );
    }

    glGenTextures( 1, tex );
    glActiveTexture( GL_TEXTURE0 );
    glBindTexture( GL_TEXTURE_2D, *tex );
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, x, y, 0, GL_RGBA, GL_UNSIGNED_BYTE,
                  image_data );
    glGenerateMipmap( GL_TEXTURE_2D );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
    GLfloat max_aniso = 0.0f;
    glGetFloatv( GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &max_aniso );
    // set the maximum!
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, max_aniso );
    return true;
}

bool GameRenderer::Init(int argc, char* argv[])
{
    GLfloat line_points[] = { 0.0f, 0.0f, 0.0f, 10.0f, 0.0f, 0.0f };
    GLfloat triangle_points[] = { 0.0f, 0.5f, 0.0f, 0.5f, -0.5f, 0.0f, -0.5f, -0.5f, 0.0f };
    GLfloat screen_points[] = { -0.5f, -0.5f, 0.0f, 0.5f,	-0.5f, 0.0f, 0.5f,	0.5f,	0.0f,
                                0.5f,	0.5f,	0.0f, -0.5f, 0.5f,	0.0f, -0.5f, -0.5f, 0.0f };
    GLfloat texcoords[] = { 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
                            1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f };

    GLfloat* vp       = NULL; // array of vertex points
    GLfloat* vn       = NULL; // array of vertex normals
    GLfloat* vt       = NULL; // array of texture coordinates
    int g_point_count = 0;
    if ( !LoadObjFile( MESH_FILE, vp, vt, vn, g_point_count ) ) {
        printf( "ERROR: loading mesh file\n" );
        return 1;
    }

    for(int i=0; i<SPHERE_COUNT; ++i) {
        glm::vec3 position = glm::vec3(-700.0f + 3*i*SPHERE_SCALE_FACTOR, 0.0f, 1500.0f);
        Sphere *sphere = new Sphere();
        sphere->sphere_position = position;
        sphere->sphere_id=i;
        // sphere->data = "Random Data: " + std::to_string(sphere->sphere_id) + " ";
        spheres.push_back(*sphere);
    }

    /*for(int i=0; i<RANDOM_SPHERE_COUNT; ++i) {
        glm::vec3 pos = glm::vec3(-100.0f + 3*i*SPHERE_SCALE_FACTOR, 0.0f, 900.0f);
        random_spheres.push_back(pos);
    }*/

    glGenBuffers(1, &m_spherevp_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, m_spherevp_vbo);
    glBufferData(GL_ARRAY_BUFFER, 3 * g_point_count * sizeof(GLfloat), vp, GL_DYNAMIC_DRAW);
    glGenBuffers(1, &m_spherevn_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, m_spherevn_vbo);
    glBufferData(GL_ARRAY_BUFFER, 3 * g_point_count * sizeof(GLfloat), vn, GL_DYNAMIC_DRAW);
    glGenBuffers(1, &m_spherevt_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, m_spherevt_vbo);
    glBufferData(GL_ARRAY_BUFFER, 2 * g_point_count * sizeof(GLfloat), vt, GL_DYNAMIC_DRAW);

    glGenBuffers(1, &m_hptr_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, m_hptr_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangle_points), triangle_points, GL_DYNAMIC_DRAW);
    glGenBuffers(1, &m_hline_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, m_hline_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(line_points), line_points, GL_DYNAMIC_DRAW);

    glGenBuffers( 1, &m_bckgrnd_vbo );
    glBindBuffer( GL_ARRAY_BUFFER, m_bckgrnd_vbo );
    glBufferData( GL_ARRAY_BUFFER, 18 * sizeof( GLfloat ), screen_points, GL_STATIC_DRAW );
    glGenBuffers( 1, &m_bckgrndtex_vbo );
    glBindBuffer( GL_ARRAY_BUFFER, m_bckgrndtex_vbo );
    glBufferData( GL_ARRAY_BUFFER, 12 * sizeof( GLfloat ), texcoords, GL_STATIC_DRAW );

    glGenVertexArrays(1, &m_sphere_vao);
    glBindVertexArray(m_sphere_vao);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, m_spherevp_vbo);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, m_spherevn_vbo);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, m_spherevt_vbo);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glBindVertexArray(0);

    glGenVertexArrays(1, &m_ddraw_vao);
    glBindVertexArray(m_ddraw_vao);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, m_hptr_vbo);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, m_hline_vbo);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glBindVertexArray(0);

    glGenVertexArrays( 1, &m_bckgrnd_vao );
    glBindVertexArray( m_bckgrnd_vao );
    glBindBuffer( GL_ARRAY_BUFFER, m_bckgrnd_vbo );
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, NULL );
    glBindBuffer( GL_ARRAY_BUFFER, m_bckgrndtex_vbo );
    glVertexAttribPointer( 1, 2, GL_FLOAT, GL_FALSE, 0, NULL ); // normalise!
    glEnableVertexAttribArray( 0 );
    glEnableVertexAttribArray( 1 );

    SetupShaders("shaders/test_vs.glsl", "shaders/test_fs.glsl", &m_shader_program);
    SetupShaders("shaders/test_vs.glsl", "shaders/test_fs.glsl", &m_lineshader_program);
    SetupShaders("shaders/sphere_vs.glsl", "shaders/sphere_fs.glsl", &m_spshader_program);
    SetupShaders("shaders/texture_vs.glsl", "shaders/texture_fs.glsl", &m_texshader_program);
    // LoadTexture("dummy.jpg", &m_bckgrnd_texture);

    this->m_model_matrix = glm::mat4(1.0f);
    this->m_view_matrix = glm::mat4(1.0f);
    this->m_proj_matrix = glm::mat4(1.0f);

    GLuint model_mat_location = glGetUniformLocation(this->m_shader_program, "model_matrix");
    glUseProgram(this->m_shader_program);
    glUniformMatrix4fv(model_mat_location, 1, GL_FALSE, glm::value_ptr(this->m_model_matrix));

    return 1;
}

void GameRenderer::DrawBackgroundTexture()
{
    glEnable(GL_BLEND);

    this->m_model_matrix = glm::mat4(1.0f);
    m_model_matrix = glm::scale(m_model_matrix, glm::vec3(SPHERE_SCALE_FACTOR));
    glUseProgram(this->m_texshader_program);
    GLuint loc = glGetUniformLocation(this->m_texshader_program, "model_matrix");
    glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(this->m_model_matrix));
    GLuint view_mat_location = glGetUniformLocation(this->m_texshader_program, "view_matrix");
    glUseProgram(this->m_texshader_program);
    glUniformMatrix4fv(view_mat_location, 1, GL_FALSE, glm::value_ptr(this->m_view_matrix));
    GLuint proj_mat_location = glGetUniformLocation(this->m_texshader_program, "projection_matrix");
    glUseProgram(this->m_texshader_program);
    glUniformMatrix4fv(proj_mat_location, 1, GL_FALSE, glm::value_ptr(this->m_proj_matrix));

    glUseProgram(m_texshader_program);
    glBindTexture(GL_TEXTURE_2D, m_bckgrnd_texture);
    glBindVertexArray(m_bckgrnd_vao);
    glBindBuffer(GL_ARRAY_BUFFER, m_bckgrnd_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, m_bckgrndtex_vbo);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glUseProgram(0);

    glDisable(GL_BLEND);
}

void GameRenderer::DrawHandPointers(std::vector<openiss::OIHandData>& hands)
{
    if(!hands.empty()) {
        for(int i=0; i<hands.size(); ++i)
        {
            openiss::Point3f coordinates = hands[i].getHandPosition();
            this->m_model_matrix = glm::mat4(1.0f);
            this->m_model_matrix = glm::translate(this->m_model_matrix, glm::vec3(coordinates.x, coordinates.y, coordinates.z));
            this->m_model_matrix = glm::scale(this->m_model_matrix, glm::vec3(HANDPTR_SCALE_FACTOR));

            glUseProgram(this->m_shader_program);
            GLuint model_mat_location = glGetUniformLocation(this->m_shader_program, "model_matrix");
            glUniformMatrix4fv(model_mat_location, 1, GL_FALSE, glm::value_ptr(this->m_model_matrix));
            GLuint view_mat_location = glGetUniformLocation(this->m_shader_program, "view_matrix");
            glUseProgram(this->m_shader_program);
            glUniformMatrix4fv(view_mat_location, 1, GL_FALSE, glm::value_ptr(this->m_view_matrix));
            GLuint proj_mat_location = glGetUniformLocation(this->m_shader_program, "projection_matrix");
            glUseProgram(this->m_shader_program);
            glUniformMatrix4fv(proj_mat_location, 1, GL_FALSE, glm::value_ptr(this->m_proj_matrix));

            glUseProgram(m_shader_program);
            glBindVertexArray(m_ddraw_vao);
            glBindBuffer(GL_ARRAY_BUFFER, m_hptr_vbo);
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
            glLineWidth(4.0f);
            glDrawArrays(GL_LINE_LOOP, 0, 3);
            glBindBuffer(GL_ARRAY_BUFFER, 0);
            glUseProgram(0);
        }
    }
}

void GameRenderer::DrawSphere(glm::vec3 position)
{
    this->m_model_matrix = glm::mat4(1.0f);
    glEnable(GL_BLEND);

    m_model_matrix = glm::translate(m_model_matrix, position);
    m_model_matrix = glm::scale(m_model_matrix, glm::vec3(SPHERE_SCALE_FACTOR));

    glUseProgram(this->m_spshader_program);
    GLuint loc = glGetUniformLocation(this->m_spshader_program, "model_matrix");
    glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(this->m_model_matrix));
    GLuint view_mat_location = glGetUniformLocation(this->m_spshader_program, "view_matrix");
    glUseProgram(this->m_spshader_program);
    glUniformMatrix4fv(view_mat_location, 1, GL_FALSE, glm::value_ptr(this->m_view_matrix));
    GLuint proj_mat_location = glGetUniformLocation(this->m_spshader_program, "projection_matrix");
    glUseProgram(this->m_spshader_program);
    glUniformMatrix4fv(proj_mat_location, 1, GL_FALSE, glm::value_ptr(this->m_proj_matrix));

    glUseProgram(this->m_spshader_program);
    glBindVertexArray(m_sphere_vao);
    glBindBuffer(GL_ARRAY_BUFFER, m_spherevp_vbo);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glBindBuffer(GL_ARRAY_BUFFER, m_spherevn_vbo);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glBindBuffer(GL_ARRAY_BUFFER, m_spherevt_vbo);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glDrawArrays(GL_TRIANGLES, 0, 1440);
    glUseProgram(0);

    glDisable(GL_BLEND);
}

void GameRenderer::DrawLine(glm::vec3 pos_1, glm::vec3 pos_2) {
    this->m_model_matrix = glm::mat4(1.0f);
    glEnable(GL_BLEND);

    glUseProgram(this->m_lineshader_program);
    GLuint loc = glGetUniformLocation(this->m_lineshader_program, "model_matrix");
    glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(this->m_model_matrix));

    GLuint view_mat_location = glGetUniformLocation(this->m_lineshader_program, "view_matrix");
    glUseProgram(this->m_lineshader_program);
    glUniformMatrix4fv(view_mat_location, 1, GL_FALSE, glm::value_ptr(this->m_view_matrix));
    GLuint proj_mat_location = glGetUniformLocation(this->m_lineshader_program, "projection_matrix");
    glUseProgram(this->m_lineshader_program);
    glUniformMatrix4fv(proj_mat_location, 1, GL_FALSE, glm::value_ptr(this->m_proj_matrix));

    GLfloat hand_coords[6] = {pos_1.x, pos_1.y, pos_1.z, pos_2.x, pos_2.y, pos_2.z};
    glBindBuffer(GL_ARRAY_BUFFER, m_hline_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(hand_coords), hand_coords, GL_DYNAMIC_DRAW);

    glUseProgram(m_lineshader_program);
    glBindVertexArray(m_ddraw_vao);
    glBindBuffer(GL_ARRAY_BUFFER, m_hline_vbo);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glLineWidth(3.0f);
    glDrawArrays(GL_LINES, 0, 2);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glUseProgram(0);
    CheckGLError();

    glDisable(GL_BLEND);
}

void GameRenderer::Draw(Camera& camera)
{
    output_data = "";
    sequence_string_a = "";
    sequence_string_b = "";
    sequence.clear();
    tracker->update();
    std::vector<openiss::OIGestureData> gestures = tracker->getGestures();
    std::vector<openiss::OIHandData> hands = tracker->getHands();
    auto frame = tracker->getDepthFrame();
    auto depthPtr = frame->getDepthData();

    if(depthPtr == nullptr) { return; }

    this->m_view_matrix = camera.GetViewMatrix();
    this->m_proj_matrix = glm::perspective(glm::radians(camera.GetVerticalFieldOfView()),
                                           (float)WINDOW_WIDTH / (float)WINDOW_HEIGHT, camera.GetNearPlane(), camera.GetFarPlane());

    // DrawBackgroundTexture();
    DrawHandPointers(hands);
    CheckGLError();

    for(int i=0; i<spheres.size(); ++i)
    {
        if (spheres[i].state == Sphere_State::STATIONARY)
        {
            glUseProgram(m_spshader_program);
            glUniform3fv(glGetUniformLocation(m_spshader_program, "use_selection_color"), 1, glm::value_ptr(glm::vec3(1.0, 0.5, 0.0)));
            glUseProgram(0);
            for (auto gesture : gestures)
            {
                if (gesture.getGestureType() == openiss::GESTURE_WAVING)
                {
                    for (auto hand : hands)
                    {
                        if (std::find(used_hands.begin(), used_hands.end(), hand.getHandID()) == used_hands.end())
                        {
                            glm::vec3 dist_vec = glm::vec3(hand.getHandPosition().x, hand.getHandPosition().y, hand.getHandPosition().z) - spheres[i].sphere_position;
                            float distance = glm::sqrt(glm::dot(dist_vec, dist_vec));
                            if (distance < SPHERE_SCALE_FACTOR * 2.0f)
                            {
                                spheres[i].hand_id = hand.getHandID();
                                spheres[i].state = Sphere_State::SELECTED;
                                spheres[i].sphere_position = glm::vec3(hand.getHandPosition().x, hand.getHandPosition().y, hand.getHandPosition().z);
                                used_hands.push_back(hand.getHandID());
                            }
                        }
                    }
                }
            }
        } else if (spheres[i].state == Sphere_State::SELECTED) {
            for (auto hand : hands)
            {
                if (hand.getHandID() == spheres[i].hand_id)
                {
                    glUseProgram(m_spshader_program);
                    glUniform3fv(glGetUniformLocation(m_spshader_program, "use_selection_color"), 1, glm::value_ptr(glm::vec3(0.0, 1.0, 1.0)));
                    glUseProgram(0);
                    spheres[i].sphere_position = glm::vec3(hand.getHandPosition().x, hand.getHandPosition().y, hand.getHandPosition().z);
                    // std::cout << "Sphere[i]: " << i << std::endl;
                    // std::cout << "hand pos x: " << hand.getHandPosition().x << " y: " << hand.getHandPosition().y << " z: " << hand.getHandPosition().z << std::endl;
                }
            }

            for (int j = 0; j < spheres.size(); ++j) {
                // if (j != i) {
                if (spheres[j].hand_id != spheres[i].hand_id) {
                    glm::vec3 dist_vec = spheres[j].sphere_position - spheres[i].sphere_position;
                    float distance = glm::sqrt(glm::dot(dist_vec, dist_vec));
                    if (distance < SPHERE_SCALE_FACTOR * 2.0f) {
                        spheres[i].state = Sphere_State::ATTACHED;
                        spheres[j].state = Sphere_State::ATTACHED;
                        int f=0;
                        for(int k=0;k<observations.size();++k)
                        {
                            for(int l=0;l<observations[k].size();++l)
                            {
                                if(observations[k][l].sphere_id==spheres[j].sphere_id)
                                {
                                    observations[k].push_back(spheres[i]);
                                    f=1;
                                }
                            }
                        }
                        if(f==0)
                        {
                            std::vector<Sphere> vec;
                            vec.push_back(spheres[i]);
                            vec.push_back(spheres[j]);
                            observations.push_back(vec);
                        }
                        used_hands.erase(std::remove(used_hands.begin(), used_hands.end(), spheres[i].hand_id), used_hands.end());
                        used_hands.erase(std::remove(used_hands.begin(), used_hands.end(), spheres[j].hand_id), used_hands.end());
                        //spheres.erase(spheres.begin() + i);
                        //spheres.erase(spheres.begin() + j);
                    }
                }
            }
        }
        else if (spheres[i].state == Sphere_State::ATTACHED)
        {
            glUseProgram(m_spshader_program);
            glUniform3fv(glGetUniformLocation(m_spshader_program, "use_selection_color"), 1, glm::value_ptr(glm::vec3(1.0, 0.0, 1.0)));
            glUseProgram(0);
        }
        this->DrawSphere(spheres[i].sphere_position);
    }

    if (!observations.empty())
    {
        for(int i=0; i<observations.size(); ++i) {
            for(int k=0;k<observations[i].size();++k)
            {
                output_data.append(observations[i][k].data);
                sequence.push_back(observations[i][k].sphere_id);
                this->DrawSphere(observations[i][k].sphere_position);
            }
        }
        for(int i=0; i < observations.size(); i++)
        {
            for(int k=0;k<observations[i].size()-1;k++)
            {
                DrawLine(glm::vec3(observations[i][k].sphere_position.x, observations[i][k].sphere_position.y, observations[i][k].sphere_position.z-200.0f),
                         glm::vec3(observations[i][k+1].sphere_position.x, observations[i][k+1].sphere_position.y, observations[i][k+1].sphere_position.z-200.0f));
            }
        }
    }

    sequence_string_a="{";
    sequence_string_b="{";
    for(int i=0;i<spheres.size();i++)
    {
        int fa=0;
        for(int k=0;k<observations.size();k++)
        {
            for(int l=0;l<observations[k].size();l++)
            {
                if(spheres[i].sphere_id==observations[k][l].sphere_id)
                {
                    fa=1;
                }
            }
        }
        if(fa==0)
        {
            sequence_string_b+="o"+std::to_string(spheres[i].sphere_id)+" fby ";
        }
    }
    for(int k=0;k<observations.size();k++)
    {
        for(int l=0;l<observations[k].size();l++)
        {
            sequence_string_a+="o"+std::to_string(observations[k][l].sphere_id)+", ";
        }
    }
}

//*********************************************************************************
// OPENGL SCENE
//*********************************************************************************

Scene::Scene(std::string title) :
        m_window(nullptr),
        m_context(nullptr),
        m_window_title(title)
{
    this->m_renderers.push_back(new GameRenderer());
    this->m_camera = new Camera();
}

bool Scene::Init(int argc, char* argv[], const std::string& config)
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        fprintf(stderr, "Failed to initialize SDL video\n");
        return 0;
    }

    m_window = SDL_CreateWindow(
            this->m_window_title.c_str(),
            SDL_WINDOWPOS_CENTERED,
            SDL_WINDOWPOS_CENTERED,
            WINDOW_WIDTH, WINDOW_HEIGHT,
            SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE );
    if (m_window == NULL) {
        fprintf(stderr, "Failed to create main window\n");
        SDL_Quit();
        return 0;
    }

    this->InitGL();
    for(const auto& renderer : m_renderers) {
        renderer->Init(argc, argv);
    }

    return 1;
}

bool Scene::InitGL()
{
    const char* glsl_version = "#version 410";
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 6);
    SDL_GL_SetAttribute(
            SDL_GL_CONTEXT_PROFILE_MASK,
            SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 5);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 5);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 5);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);

    m_context = SDL_GL_CreateContext(m_window);
    if (m_context == NULL) {
        fprintf(stderr, "Failed to create GL context\n");
        SDL_DestroyWindow(m_window);
        SDL_Quit();
        return 1;
    }

    glewExperimental = GL_TRUE;
    glewInit();
    const GLubyte *gl_renderer = glGetString(GL_RENDERER);
    const GLubyte *gl_version = glGetString(GL_VERSION);
    printf("Renderer: %s\n", gl_renderer);
    printf("OpenGL version supported %s\n", gl_version);

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    // Setup Platform/Renderer bindings
    ImGui_ImplSDL2_InitForOpenGL(m_window, m_context);
    ImGui_ImplOpenGL3_Init(glsl_version);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glEnable(GL_CULL_FACE);
    // anti-aliasing
    glEnable(GL_MULTISAMPLE);
    // transparency according to alpha values
    // glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.95f, 0.95f, 0.95f, 1.0f);

    return 1;
}

void Scene::Run()
{
    bool done = false;
    int frame_counter = 0;
    openiss::FPSCounter fps_counter;
    fps_counter.Start();

    while(!done) {
        SDL_Event event;
        while(SDL_PollEvent(&event)) {
            ImGui_ImplSDL2_ProcessEvent(&event);
            if (event.type == SDL_QUIT)
                done = true;
            if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE)
                done = true;
            if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE && event.window.windowID == SDL_GetWindowID(m_window))
                done = true;
            if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED && event.window.windowID == SDL_GetWindowID(m_window))
            {
                WINDOW_WIDTH = event.window.data1;
                WINDOW_HEIGHT = event.window.data2;
            }
        }

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplSDL2_NewFrame(m_window);
        ImGui::NewFrame();
        ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
        //ImGui::SameLine();
        if(ImGui::Button("Generate File")) {
            sequence_string_a.erase(sequence_string_a.length() - 2);
            sequence_string_b.erase(sequence_string_b.length() - 5);
            SaveToFile("/tmp/observation.ipl",
                       "os\n"
                       "where\n"
                       "  observation o0 = (\"A printed\" => \"very well\", 1, 0, 0.85, 1234);\n"
                       "  observation o1 = (\"B printed\" => \"well\", 1, 0, 0.65, 2341);\n"
                       "  observation o2 = (\"C printed\" => \"not very well\", 1, 0, 0.35, 3412);\n"
                       "  observation o3 = (\"D printed\" => \"not well\", 1, 0, 0.15, 4123);\n"
                       "  observation sequence os1 = " + sequence_string_a + "};\n"
                                                                             "  observation sequence os2 = " + sequence_string_b + "};\n"
                                                                                                                                   "  evidential statement es1 = {os1, os2};\n"
                                                                                                                                   "  os = os2;\n"
                                                                                                                                   "end");
        }
        if(ImGui::Button("Build File")) {
            system("java -jar /tmp/gipc.jar --flucid /tmp/observation.ipl");
        }
        //ImGui::End();
        ImGui::Render();

        ++frame_counter;
        float avg_fps = frame_counter / (fps_counter.GetTicks() / 1000.0f);
        std::ofstream results;
        results.open ("digieviss-nuitrack-rFPS.csv", std::ios_base::app);
        results << avg_fps << "\n";
        results.close();
        this->m_camera->UpdateLookAt();
        glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
        for(auto renderer : this->m_renderers) {
            renderer->Draw(*this->m_camera);
        }
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
        SDL_GL_SwapWindow(m_window);
    }
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplSDL2_Shutdown();
    ImGui::DestroyContext();
}

void Scene::Shutdown() {}