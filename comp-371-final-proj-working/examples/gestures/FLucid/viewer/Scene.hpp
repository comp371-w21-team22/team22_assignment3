// Some boilerplate code in Scene.hpp reused from https://github.com/daemonslayer/OpenGLZero.git
#pragma once
#ifndef OPENISS_DIGIEVISS_SCENE_H
#define OPENISS_DIGIEVISS_SCENE_H

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <exception>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <GL/glew.h>
#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/vec3.hpp>
#include "OIUtils.hpp"
#include "Sphere.hpp"

#include "OIHandData.hpp"
#include "OIDepthFrame.hpp"
#include "OINullGestureTracker.hpp"

#ifdef OPENISS_NITE2_SUPPORT
#include "OINiTEGestureTracker.hpp"
#endif

#ifdef OPENISS_NUITRACK_SUPPORT
#include "OINuiTrackGestureTracker.hpp"
#endif

#define GLM_ENABLE_EXPERIMENTAL

extern int WINDOW_WIDTH;
extern int WINDOW_HEIGHT;
constexpr float NEAR_PLANE = 10.0f;
//constexpr float FAR_PLANE = 150000.0f;
constexpr float FAR_PLANE = 15000.0f;
constexpr float HFOV = 86.0f;
constexpr float VFOV = 57.0f;
constexpr float CAMERA_SPEED = 1.0f;

// Camera Class
// Performs View Transformation : 
// Camera Matrix is used to check which objects are in view.
// Everything except those objects are clipped. For more info on such matrices,
// check clipping planes of camera {far and near plane}.

class Camera {
 private:
  float horizontal_angle;
  float vertical_angle;
  float near_plane;
  float far_plane;
  float h_fov;
  float v_fov;
  float viewport_aspect_ratio;
  float camera_speed;

  glm::vec3 position;
  glm::vec3 direction;
  glm::vec3 right;
  glm::vec3 up_vector;

 public:
  Camera(glm::vec3 position = glm::vec3(0.0f, 0.0f, -1000.0f),
         glm::vec3 direction = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f));
  Camera(float posX, float posY, float posZ, float centerX, float centerY, float centerZ, float upX, float upY, float upZ);
  virtual ~Camera() = default;

  glm::vec3 GetPosition() const;
  void SetPosition(const glm::vec3& position);
  void OffsetPosition(const glm::vec3& offset_value);

  glm::vec3 GetDirection() const;
  void SetDirection(const glm::vec3& direction);

  float GetHorizontalFieldOfView() const;
  void SetHorizontalFieldOfView(float fov);
  float GetVerticalFieldOfView() const;
  void SetVerticalFieldOfView(float fov);

  float GetNearPlane() const;
  void SetNearPlane(float near_plane);
  float GetFarPlane() const;
  void SetFarPlane(float far_plane);
  void SetNearFarPlanes(float near_plane, float far_plane);

  float GetViewportAspectRatio() const;
  void SetViewportAspectRatio(float viewport_aspect_ratio);

  float GetCameraSpeed() const;
  void SetCameraSpeed(float speed);
  void UpdateLookAt();

  glm::mat4 GetViewMatrix() const;
};

// Renderer class
// Base Class that defines properties needed for all renderers
class Renderer {
 public:
  virtual bool Init(int argc, char* argv[]) = 0;
  virtual void Draw(Camera& camera) = 0;

};

// GameRenderer class
// Renders all spheres
class GameRenderer : public Renderer {
 private:
  GLuint      m_ddraw_vao;
  GLuint      m_sphere_vao;
  GLuint      m_bckgrnd_vao;
  GLuint      m_spherevp_vbo;
  GLuint      m_spherevn_vbo;
  GLuint      m_spherevt_vbo;
  GLuint      m_hptr_vbo;
  GLuint      m_hline_vbo;
  GLuint      m_bckgrnd_vbo;
  GLuint      m_bckgrndtex_vbo;
  GLuint      m_shader_program;
  GLuint      m_lineshader_program;
  GLuint      m_spshader_program;
  GLuint      m_texshader_program;
  GLuint      m_bckgrnd_texture;
  glm::mat4   m_model_matrix;
  glm::mat4   m_view_matrix;
  glm::mat4   m_proj_matrix;
  bool if_sphere_selected;
  bool make_sphere_stationary;

  std::vector<Sphere> spheres;
  std::vector<std::vector<Sphere>> observations;
  std::vector<int> used_hands;

//  std::vector<glm::vec3> spheres;
  std::vector<glm::vec3> random_spheres;
  openiss::OIGestureTracker*  tracker;

 public:
  GameRenderer();
  virtual ~GameRenderer() = default;

  bool Init(int argc, char* argv[]) override;
  void Draw(Camera& camera) override;
  void SetupShaders(const char* vshader_fname, const char* fshader_fname, GLuint* shader_program);
  bool LoadTexture(const char* filename, GLuint* tex);
  void DrawBackgroundTexture();
  void DrawHandPointers(std::vector<openiss::OIHandData>&);
  void DrawSphere(glm::vec3 position);
  void DrawLine(glm::vec3 pos_1, glm::vec3 pos_2);
};

extern std::string output_data;
extern std::vector<int> sequence;
extern std::string sequence_string_a;
extern std::string sequence_string_b;

// Scene class
// It contains all renderers present in a level
class Scene {
 private:
  SDL_Window              *m_window;
  SDL_GLContext            m_context;
  std::string              m_window_title;
  std::vector<Renderer*>   m_renderers;
  GLuint                   m_fbo;
  GLuint                   m_fbo_tex;
  GLuint                   m_fbo_rb;
  Camera                  *m_camera;

 public:
  Scene(std::string title = "Forensic LUCID Application");
  virtual ~Scene() = default;

  bool Init(int argc, char* argv[], const std::string& config = "");
  bool InitGL();
  void Run();
  void Shutdown();
};

#endif // OPENISS_DIGIEVISS_SCENE_H
