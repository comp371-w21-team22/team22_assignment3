#version 410

out vec4 frag_colour;
in vec3 color_sel;
void main ()
{
    frag_colour = vec4(1.0f*(1-color_sel.r), 1.0f*(1-color_sel.g), 1.0f*(1-color_sel.b), 0.6f);
    // frag_colour= vec4(0.0f , 0.5f, 1.0f, 0.6f); initial blue color!
};