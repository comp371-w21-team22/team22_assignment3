#version 410

layout(location = 0) in vec3 vp;

uniform mat4 model_matrix;
uniform mat4 view_matrix;
uniform mat4 projection_matrix;

void main () 
{
    gl_Position = projection_matrix * view_matrix * model_matrix * vec4(vp, 1.0f);
}