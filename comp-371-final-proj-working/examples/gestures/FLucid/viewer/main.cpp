#include "Scene.hpp"
int main(int argc, char *argv[]) {
	Scene* scene = new Scene("DigiEVISS");
	if(!scene->Init(argc, argv)) {
		scene->Shutdown();
		return 0;
	}

	scene->Run();
	scene->Shutdown();
	return 1;
}