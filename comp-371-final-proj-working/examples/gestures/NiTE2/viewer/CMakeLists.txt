#
AUX_SOURCE_DIRECTORY(. SOURCE_FILES)

include_directories(${PROJECT_SOURCE_DIR}/src)
include_directories(${PROJECT_SOURCE_DIR}/include)

add_executable(NiTE2SampleViewer ${SOURCE_FILES})

target_link_libraries(NiTE2SampleViewer openiss)

if(ENABLE_NITE2)
    find_package(OpenNI2 REQUIRED)
    find_package(NiTE2 REQUIRED)
endif()

if(OPENNI2_LIBS_FOUND)
    target_include_directories(NiTE2SampleViewer PUBLIC ${OPENNI2_INCLUDE_DIR})
	target_link_libraries(NiTE2SampleViewer ${OPENNI2_LIBS})
endif(OPENNI2_LIBS_FOUND)

if(NITE2_FOUND)
    target_include_directories(NiTE2SampleViewer PUBLIC ${NITE2_INCLUDE_DIR})
	target_link_libraries(NiTE2SampleViewer ${NITE2_LIBRARIES})
endif(NITE2_FOUND)

# Link common relevant libraries
include(utilities)
set(is_debug 1)

link_sample_basic_libs("NiTE2SampleViewer")

# EOF
