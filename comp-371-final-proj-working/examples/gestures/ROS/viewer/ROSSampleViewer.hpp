//
// Created by jash on 07/02/19.
//

#ifndef OPENISS_ROS_SAMPLE_VIEWER_H
#define OPENISS_ROS_SAMPLE_VIEWER_H

#include <iostream>
#include <string>
#include <map>

#if defined(__APPLE__)
#include <GLUT/glut.h>
#else
#include <GL/gl.h>
#include <GL/glut.h>
#endif

#include "OIUtils.hpp"
#include "OIDepthFrame.hpp"
#include "OINullGestureTracker.hpp"

#ifdef OPENISS_NITE2_SUPPORT
#include "OINiTEGestureTracker.hpp"
#endif

#ifdef OPENISS_NUITRACK_SUPPORT
#include "OINuiTrackGestureTracker.hpp"
#endif

using namespace openiss;

class ROSSampleViewer final
{
public:
    ROSSampleViewer();
    ~ROSSampleViewer();

    // Initialize sample: initialize ROS, create all required modules,
    // register callbacks and start modules.
    void init(const std::string& config = "");

    // Update the depth map, tracking and gesture recognition data,
    // then redraw the view
    bool update();
    // Release all sample resources
    void release();

private:
    //
    int _width, _height;

#ifdef OPENISS_NITE_SUPPORT
    OIGestureTracker* m_GestureTracker = new OIROSNiTEGestureAdapter();
#elif OPENISS_NUITRACK_SUPPORT
    OIGestureTracker* m_GestureTracker = new OIROSNuiTrackGestureAdapter();
#else
    OIGestureTracker* m_GestureTracker = new OINullGestureAdapter();
#endif

    OIDepthFrame m_GFrame;

    // GL data
    GLuint _textureID;
    uint8_t* _textureBuffer;
    GLfloat _textureCoords[8];
    GLfloat _vertexes[8];
    //
    bool _isInitialized;
    int Power2(int n);
    //
    void OnNewDepthFrame(OIDepthFrame &frame);
    void InitTexture(int width, int height);
    void RenderTexture();
    void DrawTrail(OIDepthFrame &frame);
    void DrawHistory(OIGestureTracker* pHandTracker, int id, HistoryBuffer<20>* pHistory, OIDepthFrame &frame);
};

#endif // OPENISS_ROS_SAMPLE_VIEWER_H
