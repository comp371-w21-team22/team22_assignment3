#include <iostream>
#include <fstream>
#include <GL/glut.h>
#include "ROSSampleViewer.hpp"

ROSSampleViewer sample;
int start_time = time(nullptr);
int end_time;
int frame_count = 0;
float avg_fps = 0.0f;

// Keyboard handler
void keyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
        // On Esc key press
        case 27:
        {
            sample.release();
            glutDestroyWindow(glutGetWindow());
            exit(EXIT_FAILURE);
        }
        default:
        {
            // Do nothing otherwise
            break;
        }
    }
}

// Update tracking data and visualize it
void display()
{
    // Delegate this action to example's main class
    bool update = sample.update();

    if (!update)
    {
        // End the work if update failed
        sample.release();
        glutDestroyWindow(glutGetWindow());
        exit(EXIT_FAILURE);
    }

    // Do flush and swap buffers to update viewport
    glFlush();
    glutSwapBuffers();
    frame_count++;
    end_time = time(nullptr);
    if(end_time - start_time > 0)
    {
        // std::cout << "Frame Count: " << frame_count << std::endl;
        std::cout << "FPS: " << frame_count / (end_time - start_time) << std::endl;
        /*avg_fps = frame_count / (end_time - start_time);
        std::ofstream results;
        results.open ("fps_ros.csv", std::ios_base::app);
        results << avg_fps << "\n";
        results.close();*/
    }
}

void idle()
{
    glutPostRedisplay();
}

int main(int argc, char* argv[])
{
    // Prepare sample to work
    if (argc < 2)
        sample.init();
    else
        sample.init(argv[1]);

    // Initialize GLUT window
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(640, 480);
    glutCreateWindow("OpenISS Sample Viewer: OIROSAdapter");

    // Connect GLUT callbacks
    glutKeyboardFunc(keyboard);
    glutDisplayFunc(display);
    glutIdleFunc(idle);

    // Setup OpenGL
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);

    glEnableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);


    glOrtho(0, 640, 480, 0, -1.0, 1.0);
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();

    // Start main loop
    glutMainLoop();

    return 0;
}