#include "ROSSampleViewer.hpp"
#include "OIUtils.hpp"

constexpr  int WINDOW_WIDTH = 640;
constexpr int WINDOW_HEIGHT = 480;
std::map<int, openiss::HistoryBuffer<20> *> g_histories;

//*********************************************************************************
// OPENGL UTILITY CODE
//*********************************************************************************

const char * GetGLErrorStr(GLenum err)
{
    switch (err)
    {
        case GL_NO_ERROR:          return "No error";
        case GL_INVALID_ENUM:      return "Invalid enum";
        case GL_INVALID_VALUE:     return "Invalid value";
        case GL_INVALID_OPERATION: return "Invalid operation";
        case GL_STACK_OVERFLOW:    return "Stack overflow";
        case GL_STACK_UNDERFLOW:   return "Stack underflow";
        case GL_OUT_OF_MEMORY:     return "Out of memory";
        default:                   return "Unknown error";
    }
}

void CheckGLError()
{
    while (true)
    {
        const GLenum err = glGetError();
        if (GL_NO_ERROR == err)
            break;

        std::cout << "GL Error: " << GetGLErrorStr(err) << std::endl;
    }
}


// Constructor
ROSSampleViewer::ROSSampleViewer() :
        _textureID(0),
        _textureBuffer(0),
        _width(640),
        _height(480),
        _isInitialized(false)
{}

// Destructor
ROSSampleViewer::~ROSSampleViewer() {}

// Initialize ROSAdapter Modules here.
void ROSSampleViewer::init(const std::string& config)
{
    // Initialize gesture tracker.
    // Create all required modules.
    m_GestureTracker->init();
    m_GestureTracker->startGestureDetection();
    // x*y
    _width = WINDOW_WIDTH;
    _height = WINDOW_HEIGHT;
}

bool ROSSampleViewer::update()
{
    if (!_isInitialized)
    {
        // Create texture by DepthSensor
        InitTexture(_width, _height);
        _isInitialized = true;
    }
    // Wait and update ROSAdapter Depth data
    OnNewDepthFrame(m_GFrame);

    glClearColor(1, 1, 1, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Render Depth data.
    RenderTexture();
    DrawTrail(m_GFrame);

    return true;
}

void ROSSampleViewer::release()
{
    // Release ROSAdapter and remove all modules
    // m_GestureTracker->stop();
    _isInitialized = false;
    // Free texture buffer
    if (_textureBuffer)
    {
        delete[] _textureBuffer;
        _textureBuffer = 0;
    }
}

void ROSSampleViewer::DrawTrail(openiss::OIDepthFrame &frame)
{
    auto l_gestures = m_GestureTracker->getGestures();
    auto l_hands = m_GestureTracker->getHands();

    for (auto gesture : l_gestures)
    {
        std::cout << "Detected gesture of type : " << gesture.getGestureType() << std::endl;
    }

    for(auto& hand : l_hands)
    {
        //std::cout << hand.getHandPosition().x << " ," << hand.getHandPosition().y << " ," << hand.getHandPosition().z << std::endl;
        if (!hand.isHandTracked() && !hand.isHandNew() ) {
            printf("Lost hand %d\n", hand.getHandID());
            auto id = hand.getHandID();
            openiss::HistoryBuffer<20> *pHistory = g_histories[id];
            g_histories.erase(g_histories.find(id));
            delete pHistory;
        }
        else
        {
            if (hand.isHandNew()) {
                printf("Found hand %d\n", hand.getHandID());
                g_histories[hand.getHandID()] = new openiss::HistoryBuffer<20>;
            }
            // Add to history
            openiss::HistoryBuffer<20> *pHistory = g_histories[hand.getHandID()];
            if(pHistory == nullptr)
                return;
            pHistory->AddPoint(hand.getHandPosition());
            // Draw history
            DrawHistory(m_GestureTracker, hand.getHandID(), pHistory, frame);
        }
    }
}

// Copy depth frame data, received from ROSAdapter, to texture to visualize
void ROSSampleViewer::OnNewDepthFrame(openiss::OIDepthFrame &frame)
{
    m_GestureTracker->update();
    frame = *m_GestureTracker->getDepthFrame();

    uint8_t* texturePtr = _textureBuffer;
    const uint16_t* depthPtr = frame.getDepthData();
    if(depthPtr != nullptr)
    {
        float wStep = (float) _width / frame.getWidth();
        float hStep = (float) _height / frame.getHeight();

        float nextVerticalBorder = hStep;

        for (size_t i = 0; i < _height; ++i) {
            if (i == (int) nextVerticalBorder) {
                nextVerticalBorder += hStep;
                depthPtr += frame.getWidth();
            }

            int col = 0;
            float nextHorizontalBorder = wStep;
            uint16_t depthValue = *depthPtr >> 5;

            for (size_t j = 0; j < _width; ++j, texturePtr += 3) {
                if (j == (int) nextHorizontalBorder) {
                    ++col;
                    nextHorizontalBorder += wStep;
                    depthValue = *(depthPtr + col) >> 5;
                }

                texturePtr[0] = depthValue;
                texturePtr[1] = depthValue;
                texturePtr[2] = depthValue;
            }
        }
    }
}

// Render prepared background texture
void ROSSampleViewer::RenderTexture()
{
    glEnable(GL_TEXTURE_2D);
    glColor4f(1, 1, 1, 1);

    glBindTexture(GL_TEXTURE_2D, _textureID);
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, _width, _height, GL_RGB, GL_UNSIGNED_BYTE, _textureBuffer);

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    glVertexPointer(2, GL_FLOAT, 0, _vertexes);
    glTexCoordPointer(2, GL_FLOAT, 0, _textureCoords);

    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDisable(GL_TEXTURE_2D);
}

int ROSSampleViewer::Power2(int n)
{
    unsigned int m = 2;
    while (m < n)
        m <<= 1;

    return m;
}

void ROSSampleViewer::InitTexture(int width, int height)
{
    glGenTextures(1, &_textureID);

    width = Power2(width);
    height = Power2(height);

    if (_textureBuffer != 0)
        delete[] _textureBuffer;

    _textureBuffer = new uint8_t[width * height * 3];
    memset(_textureBuffer, 0, sizeof(uint8_t) * width * height * 3);

    glBindTexture(GL_TEXTURE_2D, _textureID);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Set texture coordinates [0, 1] and vertexes position
    {
        _textureCoords[0] = (float) _width / width;
        _textureCoords[1] = (float) _height / height;
        _textureCoords[2] = (float) _width / width;
        _textureCoords[3] = 0.0;
        _textureCoords[4] = 0.0;
        _textureCoords[5] = 0.0;
        _textureCoords[6] = 0.0;
        _textureCoords[7] = (float) _height / height;

        _vertexes[0] = _width;
        _vertexes[1] = _height;
        _vertexes[2] = _width;
        _vertexes[3] = 0.0;
        _vertexes[4] = 0.0;
        _vertexes[5] = 0.0;
        _vertexes[6] = 0.0;
        _vertexes[7] = _height;
    }
}

float Colors[][3] = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}, {1, 1, 1}};
int colorCount = 3;

void ROSSampleViewer::DrawHistory(openiss::OIGestureTracker* pHandTracker, int id, openiss::HistoryBuffer<20>* pHistory, openiss::OIDepthFrame &frame)
{
    glColor3f(Colors[id % colorCount][0], Colors[id % colorCount][1], Colors[id % colorCount][2]);
    float coordinates[60] = {0};
    float factorX = WINDOW_WIDTH / (float)frame.getWidth();
    float factorY = WINDOW_HEIGHT / (float)frame.getHeight();

    for (int i = 0; i < pHistory->GetSize(); ++i)
    {
        const openiss::Point3f& position = pHistory->operator[](i);
        pHandTracker->convertHandCoordinatesToDepth(position.x, position.y, position.z, &coordinates[i*3], &coordinates[i*3+1]);

        coordinates[i*3]   *= factorX;
        coordinates[i*3+1] *= factorY;
    }

    CheckGLError();
    glPointSize(18);
    glVertexPointer(3, GL_FLOAT, 0, coordinates);
    glDrawArrays(GL_LINE_STRIP, 0, pHistory->GetSize());

    glPointSize(12);
    glVertexPointer(3, GL_FLOAT, 0, coordinates);
    glDrawArrays(GL_POINTS, 0, 1);
    CheckGLError();
}