//
// Created by jash on 25/01/19.
//

#include "OINullGestureTracker.hpp"

#ifdef OPENISS_NUITRACK_SUPPORT
#include "OINuiTrackGestureTracker.hpp"
#endif

using namespace openiss;

int main(int argc, char** argv)
{
#ifdef OPENISS_NUITRACK_SUPPORT
  OIGestureTracker* gesture_tracker = new OINuiTrackGestureTracker();
#else
  OIGestureTracker* gesture_tracker = new OINullGestureTracker();
#endif

  gesture_tracker->init();
  gesture_tracker->startGestureDetection();

  while(true)
  {
    gesture_tracker->update();
    auto gestures = gesture_tracker->getGestures();
    auto hands = gesture_tracker->getHands();

    for(auto gesture : gestures)
    {
      std::cout << "Detected gesture of type : " << gesture.getGestureType() << std::endl;
    }

    for(auto hand : hands)
    {
      if(hand.isHandTracked())
        std::cout << "Tracking hand with id : " << hand.getHandID() << std::endl;
    }
  }
  return 0;
}