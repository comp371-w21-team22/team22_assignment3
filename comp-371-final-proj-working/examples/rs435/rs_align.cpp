//
// Created by Haotao Lai on 2018-10-24.
//

#include "OpenISS.hpp"
#include "OIAligner.hpp"
#include "OIDataFrame.hpp"

#include <opencv2/opencv.hpp>

using namespace openiss;

int main(int argc, char** argv)
{
    const char* win1Name = "color image";
    const char* win2Name = "depth image";
    const char* win3Name = "aligned image";

    cv::namedWindow(win1Name);
    cv::namedWindow(win2Name);
    cv::namedWindow(win3Name);

    OIDeviceFactory factory;

#ifdef OPENISS_REALSENSE_SUPPORT
    std::shared_ptr<OIDevice> pDevice = factory.create("rs_d435");
#else
    std::shared_ptr<OIDevice> pDevice = factory.create("null");
#endif

    pDevice->open();
    pDevice->enable();

    OIAligner aligner;
    int h = pDevice->getIntrinsic(DEPTH_STREAM).height;
    int w = pDevice->getIntrinsic(DEPTH_STREAM).width;

    byte* alignedDataBuf{nullptr};
    bool shutdown = false;
    
    while (!shutdown)
    {
        OIFrame* colorFrame = pDevice->readFrame(openiss::COLOR_STREAM);
        OIFrame* depthFrame = pDevice->readFrame(openiss::DEPTH_STREAM);
        colorFrame->show(win1Name);
        depthFrame->show(win2Name);

        // prepare required inputs
        auto pf = (OIDataFrame*)depthFrame;
        int bbp = pf->getBytesPerPixel();
        int len = bbp * h * w;
        
        if(alignedDataBuf == nullptr)
        {
            alignedDataBuf = new byte[len];
        }

        // obtains the result and displays it
        aligner.deprojectImageToPointCloud(pDevice.get(), pf, alignedDataBuf, len);
        OIDataFrame alignedFame(DEPTH_FRAME, alignedDataBuf, bbp, w, h);
        alignedFame.show(win3Name);

        // shutdown on esc
        int key = cv::waitKey(1);
        shutdown = shutdown || (key > 0 && ((key & 0xFF) == 27));
    }

    delete[] alignedDataBuf;
    return 0;
}

// EOF
