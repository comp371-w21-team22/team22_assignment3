//
// This file is an example template for OpenISS project
//

#include "OpenISS.hpp"
#include "OIAligner.hpp"
#include "OIDataFrame.hpp"

#include <functional>
#include <opencv2/opencv.hpp>

using namespace openiss;

typedef std::function<void(int)> TrackbarAction;

void removeBackground
(
    OIDataFrame* otherFrame,
    const byte* depthFrame,
    float depthScale,
    float threshold
);

int main(int argc, char** argv)
{
    const char* win1Name = "original color image";
    const char* win2Name = "masked color image";
    cv::namedWindow(win1Name);
    cv::namedWindow(win2Name);

    int trackbarValue = 1;
    const int maxTrackbarValue = 5;
    std::string trackbarName = "Mask Distance";

    TrackbarAction action = [&](int pos)
    {
        trackbarValue = pos;
    };

    cv::TrackbarCallback trackbarCallback = [](int pos, void* userdata)
    {
        (*(TrackbarAction *)userdata)(pos);
    };

    cv::createTrackbar
    (
        trackbarName,
        win1Name,
        &trackbarValue,
        maxTrackbarValue,
        trackbarCallback,
        (void*)&action
    );

    OIDeviceFactory factory;

#ifdef OPENISS_REALSENSE_SUPPORT
    std::shared_ptr<OIDevice> pDevice = factory.create("rs_d435");
#else
    std::shared_ptr<OIDevice> pDevice = factory.create("null");
#endif

    pDevice->open();
    pDevice->enable();

    OIAligner aligner;
    int h = pDevice->getIntrinsic(DEPTH_STREAM).height;
    int w = pDevice->getIntrinsic(DEPTH_STREAM).width;
    float depthScale = pDevice->getDepthScale();

    byte* alignedDataBuf{nullptr};
    bool shutdown = false;

    while(!shutdown)
    {
        OIFrame* colorFrame = pDevice->readFrame(openiss::COLOR_STREAM);
        OIFrame* depthFrame = pDevice->readFrame(openiss::DEPTH_STREAM);
        colorFrame->show(win1Name);

        // prepare required inputs
        auto pf = (OIDataFrame*)depthFrame;
        int bpp = pf->getBytesPerPixel();
        int len = bpp * h * w;

        if(alignedDataBuf == nullptr)
        {
            alignedDataBuf = new byte[len];
        }

        // obtains the aligned result mask with the color image and display it
        aligner.deprojectImageToPointCloud(pDevice.get(), pf, alignedDataBuf, len);
        OIDataFrame alignedFrame(DEPTH_FRAME, alignedDataBuf, bpp, w, h);
        alignedFrame.show(win2Name);

        //        OIDataFrame maskedFrame(*((OIDataFrame*) colorFrame));
        //        removeBackground(&maskedFrame, alignedDataBuf, depthScale, trackbarValue);
        //        maskedFrame.show(win2Name);

        int key = cv::waitKey(1);
        shutdown = shutdown || (key > 0 && ((key & 0xFF) == 27)); 
    }

    // shutdown on esc
    delete[] alignedDataBuf;
    return 0;
}

void removeBackground
(
    OIDataFrame* otherFrame,
    const byte* depthFrame,
    float depthScale,
    float threshold
)
{
    int width = otherFrame->getWidth();
    int height = otherFrame->getHeight();
    int bpp = otherFrame->getBytesPerPixel();

    auto ptrDepthFrame = reinterpret_cast<const uint16_t*>(depthFrame);
    auto ptrOtherFrame = reinterpret_cast<uint8_t*>(otherFrame->getData());

    for(int y = 0; y < height; ++y)
    {
        auto depthPixelIndex = y * width;

        for(int x = 0; x < width; ++x, ++depthPixelIndex)
        {
            float dis = depthScale * ptrDepthFrame[depthPixelIndex];

            if(dis > threshold)
            {
                auto offset = depthPixelIndex * bpp;
                std::memset(&ptrOtherFrame[offset], 0xff, static_cast<size_t>(bpp));
            }

            //            if (dis <= 0.0 || dis > threshold) {
            //                auto offset = depthPixelIndex * bpp;
            //                std::memset(&ptrOtherFrame[offset], 0xff, static_cast<size_t>(bpp));
            //            }
        }
    }
}

// EOF
