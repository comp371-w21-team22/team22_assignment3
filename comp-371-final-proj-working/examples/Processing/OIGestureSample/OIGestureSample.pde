import java.util.Map;
import java.util.Iterator;
import openiss.oigesture.*;

OIGestureTracker tracker = null;
GesturesVector gestures = null;
HandsVector hands = null;

int handVecListSize = 20;
Map<Integer,ArrayList<PVector>>  hand_positions_list = new HashMap<Integer,ArrayList<PVector>>();
color[]       user_color = new color[]{ color(255,0,0),
                                     color(0,255,0),
                                     color(0,0,255),
                                     color(255,255,0),
                                     color(255,0,255),
                                     color(0,255,255)
                                   };

void setup()
{
    size(640, 480);
    //tracker = new OINiTEGestureTracker();
    tracker = new OINuitrackGestureTracker();
    tracker.init();
    tracker.startGestureDetection();
}

PImage getPImage(short[] p_asPixels)
{
  PImage oPImage = new PImage(640, 480);

  if(p_asPixels != null)
  {
    oPImage.loadPixels();

    for(int c = 0; c < p_asPixels.length; c++)
    {
      int iC = p_asPixels[c];
      iC = iC >> 5;

      oPImage.pixels[c] = color(iC, iC, iC);
    }
    oPImage.updatePixels();
  }
  return oPImage;
}

void draw()
{
  tracker.update();
  gestures = tracker.getGestures();
  hands = tracker.getHands();

  for (OIGestureData o : gestures)
  {
      println("gesture type : "+ o.getGestureType() );
  }

  for (OIHandData o : hands)
  {
      println("hand id : "+ o.getHandID());
      println("hand position x: "+ o.getHandPosition().getX() + " " +
              "hand position y: "+ o.getHandPosition().getY() + " " +
              "hand position z: "+ o.getHandPosition().getZ());

      PVector pos = new PVector(o.getHandPosition().getX(),
                                o.getHandPosition().getY(),
                                o.getHandPosition().getZ());

      if(o.isHandNew())
      {
          ArrayList<PVector> vecList = new ArrayList<PVector>();
          vecList.add(pos);
          hand_positions_list.put(o.getHandID(), vecList);
      }
      else if (o.isHandLost())
      {
          hand_positions_list.remove(o.getHandID());
      }
      else if (o.isHandTracked())
      {
          ArrayList<PVector> vecList = hand_positions_list.get(o.getHandID());
          if(vecList != null)
          {
            vecList.add(0,pos);
            if(vecList.size() >= handVecListSize)
                vecList.remove(vecList.size()-1);
          }
      }
  }

  OIDepthFrame frame = tracker.getDepthFrame();
  short [] pixel_ = frame.getDepthData();

  image(getPImage(pixel_), 0, 0);

  // println("gestures size : "+ gestures.size() );
  // println("hands size : "+ hands.size());
  // draw the tracked hands
  if(hand_positions_list.size() > 0)
  {
    Iterator itr = hand_positions_list.entrySet().iterator();
    while(itr.hasNext())
    {
      Map.Entry mapEntry = (Map.Entry)itr.next();
      int handId =  (Integer)mapEntry.getKey();
      ArrayList<PVector> vecList = (ArrayList<PVector>)mapEntry.getValue();
      float[] x = new float[1];
      float[] y = new float[1];
      PVector p;

        stroke(user_color[ (handId - 1) % user_color.length ]);
        noFill();
        strokeWeight(1);
        Iterator itrVec = vecList.iterator();
        beginShape();
          while( itrVec.hasNext() )
          {
            p = (PVector) itrVec.next();
            tracker.convertHandCoordinatesToDepth(p.x, p.y, p.z, x, y);
            vertex(x[0],y[0]);
          }
        endShape();

        stroke(user_color[ (handId - 1) % user_color.length ]);
        strokeWeight(4);
        p = vecList.get(0);
        tracker.convertHandCoordinatesToDepth(p.x, p.y, p.z, x, y);
        point(x[0],y[0]);

    }
  }
  println("FPS : "+ frameRate);
}
// E.O.F
