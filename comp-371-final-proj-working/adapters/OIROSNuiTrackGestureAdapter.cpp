//
// Created by jash on 13/05/19.
//

#include "OIROSNuiTrackGestureAdapter.hpp"

using namespace openiss;

OIStatus OIROSNuiTrackGestureAdapter::init()
{
    /**/
    int argc = 0;
    char **argv = nullptr;
    m_depth_callback_counter = 0;
    m_gesture_callback_counter = 0;
    m_hand_callback_counter = 0;
    m_msg_queue_size = 0;
    /**/
    ros::init(argc, argv, "ROSAdapter");
    if (ros::isInitialized())
    {
        std::cout << "ROS initialized!" << std::endl;
        std::cout << "Subscribing to depth frames on topic /openiss/camera/frame/depth..." << std::endl;
        std::cout << "Subscribing to gestures on topic /openiss/gestures..." << std::endl;
        std::cout << "Subscribing to hands on topic /openiss/hands..." << std::endl;

        ros::NodeHandle l_nh;
        m_DepthFrameSubscriber = l_nh.subscribe("/openiss/camera/frame/depth", m_msg_queue_size, &OIROSNuiTrackGestureAdapter::adaptDepthData, this);
        m_GesturesSubscriber = l_nh.subscribe("/openiss/gestures", m_msg_queue_size, &OIROSNuiTrackGestureAdapter::adaptGestureData, this);
        m_HandsSubscriber = l_nh.subscribe("/openiss/hands", m_msg_queue_size, &OIROSNuiTrackGestureAdapter::adaptHandData, this);

        return OIStatus::STATUS_OK;
    }
    else
        return OIStatus::STATUS_FAIL;
}

OIStatus OIROSNuiTrackGestureAdapter::update()
{
    m_eGestureData.clear();
    m_eHandData.clear();
    ros::spinOnce();
    return m_eStatus;
}

OIStatus OIROSNuiTrackGestureAdapter::stop()
{
    return m_eStatus;
}

std::vector<OIGestureData> OIROSNuiTrackGestureAdapter::getGestures()
{
    return m_eGestureData;
}

std::vector<OIHandData> OIROSNuiTrackGestureAdapter::getHands()
{
    return m_eHandData;
}

OIDepthFrame* OIROSNuiTrackGestureAdapter::getDepthFrame()
{
    return &this->m_DepthFrame;
}

void OIROSNuiTrackGestureAdapter::adaptDepthData( const openiss_gesture_tracker_msgs::DepthFrame& p_depthFrameMsg )
{
    ++m_depth_callback_counter;
    m_depthData = &p_depthFrameMsg.depth_data.front();
    m_DepthFrame.setWidth(p_depthFrameMsg.frame_width);
    m_DepthFrame.setHeight(p_depthFrameMsg.frame_height);
    m_DepthFrame.setDepthData(m_depthData);
    std::cout << "frame id: " << p_depthFrameMsg.frame_id << " timestamp: " << p_depthFrameMsg.frame_timestamp << std::endl;
    std::cout << "depth_callback_counter [rosnuitracksubscriber]: " << m_depth_callback_counter << " timestamp: " << ros::Time::now() << std::endl;
    std::cout << "delay: " << (p_depthFrameMsg.frame_timestamp.toNSec() - ros::Time::now().toNSec()) << std::endl;
}

OIGestureType convertROSGesture(uint16_t p_GestureType)
{
    switch (p_GestureType)
    {
        case 8:
            std::cout << "GESTURE_CLICK_ROS" << std::endl;
            return OIGestureType::GESTURE_PUSH;
        default:
            return OIGestureType::GESTURE_ALL;
    }
}

void OIROSNuiTrackGestureAdapter::adaptGestureData(const openiss_gesture_tracker_msgs::GestureArray& p_GesturesMsg)
{
    ++m_gesture_callback_counter;
    auto gestures = p_GesturesMsg.gestures;
    for (auto gesture : gestures)
    {
        m_eGesture.setGestureType(convertROSGesture(gesture.gesture_type));
        m_eGestureData.push_back(m_eGesture);
    }
    if(!gestures.empty())
    {
        std::cout << "pub_gesture_callback_counter [rosnuitracksubscriber]: " << p_GesturesMsg.gesture_frame_id << std::endl;
        std::cout << "sub_gesture_callback_counter [rosnuitracksubscriber]: " << m_gesture_callback_counter << std::endl;
    }
}

OIHandState convertROSHandState(u_int8_t p_ROSHandState)
{
    switch (p_ROSHandState)
    {
        case 1:
            return OIHandState::HAND_IS_NEW;
        case 2:
            return OIHandState::HAND_IS_LOST;
        case 3:
            return OIHandState::HAND_IS_TRACKING;
        default:
            return OIHandState::HAND_IS_ABSENT;
    }
}

void OIROSNuiTrackGestureAdapter::adaptHandData(const openiss_gesture_tracker_msgs::HandArray &p_HandsMsg)
{
    ++m_hand_callback_counter;
    auto hands = p_HandsMsg.hands;
    for (auto hand : hands)
    {
        m_eHand.setHandID(hand.hand_id);
        m_eHand.setHandPosition(hand.hand_position.x, hand.hand_position.y, hand.hand_position.z );
        m_eHand.setHandState(convertROSHandState(hand.hand_state));
        m_eHandData.push_back(m_eHand);
    }
    if(!hands.empty())
    {
        std::cout << "pub_hand_callback_counter [rosnuitracksubscriber]: " << p_HandsMsg.hand_frame_id << std::endl;
        std::cout << "sub_hand_callback_counter [rosnuitracksubscriber]: " << m_hand_callback_counter << std::endl;
    }
}

void OIROSNuiTrackGestureAdapter::convertHandCoordinatesToDepth(float p_x, float p_y, float p_z, float *p_OutX, float *p_OutY)
{
    /*
    *p_OutX = ((p_x)/(0.001751505728*p_z))+320;
    *p_OutY = (-(p_y)/(0.001751505728*p_z))+240;
    */
    *p_OutX = (-(p_x)/(0.001736669612*p_z))+320; // <- p_x coordinate manually negated here!
    *p_OutY = (-(p_y)/(0.001736669612*p_z))+240;
}

void OIROSNuiTrackGestureAdapter::convertDepthCoordinatesToHand(int p_x, int p_y, int p_z, float *p_OutX, float *p_OutY) {}