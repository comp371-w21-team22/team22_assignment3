//
// Created by h_lai on 13/02/19.
//

#include "OIYoloDetector.hpp"

static PyObject* fromMatToNdArray(Mat &mat);
static Mat fromNdArrayToMat(PyObject *ndarray);

static PyObject* fromMatToNdArray(Mat &mat) {
    Mat contiMat;
    // make sure mat is continuous
    if (!mat.isContinuous()) {
        contiMat = mat.clone();
    } else {
        contiMat = mat;
    }
    npy_intp dims[] = {contiMat.rows, contiMat.cols, contiMat.channels()};
    PyObject *pNdArray = PyArray_SimpleNewFromData(contiMat.channels(), dims, NPY_UINT8, contiMat.data);
    Py_INCREF(pNdArray);
    return pNdArray;
}

static Mat fromNdArrayToMat(PyObject *ndarray) {
    PyArrayObject *arr;
    PyArray_OutputConverter(ndarray, &arr);
    npy_intp *shape = PyArray_SHAPE(arr);
    Mat mat(shape[0], shape[1], CV_8UC3, PyArray_DATA(arr));
    return mat;
}

static void parseBboxResult(
        PyObject *result,
        vector<vector<openiss::Point2i>> &bbox,
        vector<float> &scores
) {
    Mat res = fromNdArrayToMat(result);
    for (int i = 0; i < res.rows; i++) {
        const float *data = res.ptr<float>(i);
        vector<openiss::Point2i> box;
        openiss::Point2i p1, p2;
        p1.y = static_cast<int &&>(data[0]);
        p1.x = static_cast<int &&>(data[1]);
        p2.y = static_cast<int &&>(data[2]);
        p2.x = static_cast<int &&>(data[3]);
        box.push_back(p1);
        box.push_back(p2);
        bbox.push_back(box);
        scores.push_back(data[4]);
    }
}

int openiss::OIYoloDetector::init() {
    char *modName = const_cast<char *>("yolo_model");
    import_array();
    env.importPyModule(modName);
    pInstance = env.createPyInstance(modName, const_cast<char *>("YOLO"), "");
    pFuncDetectImage = env.loadPyMethod(pInstance, const_cast<char *>("detect_image"));
    pFuncDetectBbox = env.loadPyMethod(pInstance, const_cast<char *>("detect_bbox"));
}

openiss::OIYoloDetector::OIYoloDetector() {
    init();
}

Mat openiss::OIYoloDetector::detect_image(Mat &input) {
    // convert the frame into python numpy format
    pImageArg = fromMatToNdArray(input);

    PyObject *pArgs = PyTuple_New(1);
    PyTuple_SetItem(pArgs, 0, pImageArg);

    // pass to python
    pResult = env.invokePyMethod(pFuncDetectImage, pArgs);
    Py_DECREF(pArgs);
    Py_DECREF(pImageArg);

    // convert the result back to C++ format
    Mat boxedImage = fromNdArrayToMat(pResult);
    Py_DECREF(pResult);
    return boxedImage;
}

openiss::OIYoloDetector::~OIYoloDetector() {
    Py_DECREF(pFuncDetectImage);
    Py_DECREF(pInstance);
}

void openiss::OIYoloDetector::detect_bbox(
        Mat &input,
        vector<vector<openiss::Point2i>> &bbox,
        vector<float> &scores
) {
    // convert the frame into python numpy format
    pImageArg = fromMatToNdArray(input);

    PyObject *pArgs = PyTuple_New(1);
    PyTuple_SetItem(pArgs, 0, pImageArg);

    // pass to python
    pResult = env.invokePyMethod(pFuncDetectBbox, pArgs);
    Py_DECREF(pArgs);
    Py_DECREF(pImageArg);

    parseBboxResult(pResult, bbox, scores);

    Py_DECREF(pResult);
}
