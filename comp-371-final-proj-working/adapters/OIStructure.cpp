#include <opencv2/core/mat.hpp>
#include <thread>

#include "OIStructure.hpp"
#include "OIFrameCVImpl.hpp"

openiss::OIStructure::OIStructure()
    : mpColorOIFrame(nullptr)
    , mpDepthOIFrame(nullptr)
    , mpIrOIFrame(nullptr)
    , streamStarted(false)
{ }

openiss::OIStructure::~OIStructure() {
    delete mpDepthOIFrame;
    delete mpColorOIFrame;
    delete mpIrOIFrame;
}

void openiss::OIStructure::init() {
    settings.source = ST::CaptureSessionSourceId::StructureCore;
    settings.structureCore.depthEnabled = true;
    settings.structureCore.visibleEnabled = true;
    settings.structureCore.infraredEnabled = true;
    settings.structureCore.accelerometerEnabled = false;
    settings.structureCore.gyroscopeEnabled = false;
    settings.structureCore.depthResolution = ST::StructureCoreDepthResolution::VGA;
    session.setDelegate(&delegate);
}

void openiss::OIStructure::open() {
    init();
    if (!session.startMonitoring(settings)) {
        printf("Failed to initialize capture session!\n");
    }
    else{
        // Wait until device starts streaming content
        while(!delegate.isStreaming){}
    }
}

openiss::OIFrame* openiss::OIStructure::readFrame(openiss::StreamType typeFrame) {
    if (typeFrame == openiss::COLOR_STREAM) {
        if(delegate.mColorFrame.timestamp() != lastColorFrameTimestamp){
            lastColorFrameTimestamp = delegate.mColorFrame.timestamp();
            delete mpColorOIFrame;
            cv::Mat img(delegate.mColorFrame.height(), delegate.mColorFrame.width(),
                        CV_8UC3, (void *) delegate.mColorFrame.rgbData());
            cv::cvtColor(img, img, CV_BGR2RGB);
            mpColorOIFrame = new OIFrameCVImpl(img);
        }
        return mpColorOIFrame;
    }
    if (typeFrame == openiss::DEPTH_STREAM) {
        if(delegate.mDepthFrame.timestamp() != lastDepthFrameTimestamp){
            lastDepthFrameTimestamp = delegate.mDepthFrame.timestamp();
            delete mpDepthOIFrame;
            cv::Mat img(delegate.mDepthFrame.height(), delegate.mDepthFrame.width(),
                        CV_16UC1, (void *) delegate.mDepthFrame.convertDepthToUShortInMillimeters());
            img.convertTo(img, CV_8U, 255.0/1000);
            cv::cvtColor(img, img, CV_GRAY2BGR);
            mpDepthOIFrame = new OIFrameCVImpl(img);
        }
        return mpDepthOIFrame;
    }
    if (typeFrame == openiss::IR_STREAM) {
        if(delegate.mInfraredFrame.timestamp() != lastInfraredFrameTimestamp){
            lastInfraredFrameTimestamp = delegate.mInfraredFrame.timestamp();
            delete mpIrOIFrame;
            cv::Mat img(delegate.mInfraredFrame.height(), delegate.mInfraredFrame.width(),
                        CV_16UC1, (void *) delegate.mInfraredFrame.data());
            img.convertTo(img, CV_8U, 255.0/1000);
            cv::resize(img, img, cv::Size(), 0.5, 0.5);
            cv::cvtColor(img, img, CV_GRAY2BGR);
            mpIrOIFrame = new OIFrameCVImpl(img);
        }
        return mpIrOIFrame;
    }

    std::cout << "[debug] not supported steam type." << std::endl;
    exit(1);
}

openiss::Intrinsic openiss::OIStructure::getIntrinsic(openiss::StreamType streamType) {
    return openiss::Intrinsic();
}

openiss::Extrinsic openiss::OIStructure::getExtrinsic(openiss::StreamType from, openiss::StreamType to) {
    return openiss::Extrinsic();
}

float openiss::OIStructure::getDepthScale() {
    return 0;
}

// ***************************************************************** //
// the following methods won't have any effect, just respect the
// super class OIFrame, but will not affect the functionality
// ***************************************************************** //
bool openiss::OIStructure::enable() {
    return false;
}

bool openiss::OIStructure::enableRegistered() {
    return false;
}

bool openiss::OIStructure::enableDepth() {
    return false;
}

bool openiss::OIStructure::enableColor() {
    return false;
}

void openiss::OIStructure::close() {

}

void *openiss::OIStructure::rawDevice() {
    return nullptr;
}
// ***************************************************************** //

