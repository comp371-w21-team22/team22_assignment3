//
// Created by Yiran Shen on 01/04/19
// 

#include "FER3D.hpp"
#include <iostream>

static PyObject* fromMatToNdArray(Mat &mat);
static Mat fromNdArrayToMat(PyObject *ndarray);

static PyObject* fromMatToNdArray(Mat &mat) {
    Mat contiMat;
    // make sure mat is continuous
    if (!mat.isContinuous()) {
        contiMat = mat.clone();
    } else {
        contiMat = mat;
    }
    npy_intp dims[] = {contiMat.rows, contiMat.cols, contiMat.channels()};
    PyObject *pNdArray = PyArray_SimpleNewFromData(contiMat.channels(), dims, NPY_UINT8, contiMat.data);
    Py_INCREF(pNdArray);
    return pNdArray;
}

static Mat fromNdArrayToMat(PyObject *ndarray) {
    PyArrayObject *arr;
    PyArray_OutputConverter(ndarray, &arr);
    npy_intp *shape = PyArray_SHAPE(arr);
    Mat mat(shape[0], shape[1], CV_32FC1, PyArray_DATA(arr));
    return mat;
}

int openiss::FER3D::init() {
    char *modName = const_cast<char *>("fer_3d");
    import_array();
    env.importPyModule(modName);
    pInstance = env.createPyInstance(modName, const_cast<char *>("FER3D"), "");
    pFuncPredict = env.loadPyMethod(pInstance, const_cast<char *>("predict"));
}

openiss::FER3D::FER3D() {
    init();
}

openiss::FER3D::~FER3D() {
    Py_DECREF(pFuncPredict);
    Py_DECREF(pInstance);
}

cv::Mat openiss::FER3D::predict(std::vector<cv::Mat>& imageVector) {
    // convert imageVector to n frame image sequence gif mat

    if (imageVector.size() != 4) {
        std::cout << "FER3D predict input must be 4 frames" << std::endl;
        cv::Mat temp;
        return temp;
    }

    // opencv can not read gif (n frame image, so we can not pass 1 gif with 8 frames but 8 frames )
    pImageArg1 = fromMatToNdArray(imageVector[0]);
    pImageArg2 = fromMatToNdArray(imageVector[1]);
    pImageArg3 = fromMatToNdArray(imageVector[2]);
    pImageArg4 = fromMatToNdArray(imageVector[3]);


    PyObject *pArgs = PyTuple_New(4);
    PyTuple_SetItem(pArgs, 0, pImageArg1);
    PyTuple_SetItem(pArgs, 1, pImageArg2);
    PyTuple_SetItem(pArgs, 2, pImageArg3);
    PyTuple_SetItem(pArgs, 3, pImageArg4);


    // pass to python
    pResult = env.invokePyMethod(pFuncPredict, pArgs);
    Py_DECREF(pArgs);
    Py_DECREF(pImageArg1);
    Py_DECREF(pImageArg2);
    Py_DECREF(pImageArg3);
    Py_DECREF(pImageArg4);


    // convert the result back to C++ format
    cv::Mat resultMat = fromNdArrayToMat(pResult);
    Py_DECREF(pResult);
    return resultMat;

}

