# COMP-371 Final Project : Philippe Vo (27759908)

## What is it about ?
- This is the final project for the COMP-371 class (Computer Graphics). 

## Bitbucket link
- You can find this project in "comp371-w21-team22/team22_assignment3" on bitbucket

### Final Project
- The topic chosen was OIFXCAM.
- The requirements was to :
  - display the contents of a webcam to a screen model, sitting on a stage model
  - allow user input to choose between :
    - normal view
    - depth view (gray scale view)
    - infared view

## Mouse and Keyboard Inputs
```
- ESC                       : close window
- AWSD                      : move left-right and forward-backward
- R                         : switch to main camera
- M                         : switch to camera placed in front of model
- B                         : switch to camera placed behind of model
- Left-Right Arrow          : switch to camera circling the center stage (press on left key first to get it going)
- X                         : switch to camera located at the main light source (main spotlight)
- 1                         : switch to normal view webcam
- 2                         : switch to depth view webcam
- 3                         : switch to infared view webcam
```
## Credits
- The final project borrows heavily from the "COMP-371 class" Lab04, Lab06 and Lab08. Some code was taken from there. It also borrows
heavily from the second assignment of "COMP-371".

## Resources used
- https://learnopengl.com/Getting-started/OpenGL

## Known Differences from instructions given
- With the lack of clear documentation on how to use openFrameworks as a third-library (most documentation points out to using their own
project system framework), I opted to use instead OpenCV to allow webcam frame capturing.

# To compile

- mkdir build && cd build
- cmake -L ..
- make
- make install
- copy assets folder into build/examples/webcam_test
- cd into build/examples/webcam_test
- ./webcam_test
