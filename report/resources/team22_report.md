# Introduction

For the assignment 3, I have been given the "OIFXCAM" topic to work on. This particular topic
required us to make a class that would allow us to take the contents of a webcam, apply it to a
texture in OpenGL and display it on a model. The user should be allowed to switch between : 
normal mode, depth mode and infared mode, using keyboard inputs

# Requirements
- fetch content from webcam and show it on a model in OpenGL
- have a webcam normal mode
- have a webcam depth mode
- have a webcam infared mode

# Design
To make the OIFXWebCam, I simply made it inherit from "OIDevice", so that it can have 
access to some helper functions like "open" and "close". From there I implemented the open() function, which
would simply get the first camera that is connected to the computer. I implemented another function called getFrame().
This function would simply retrieve a frame from the camera, and depending on the mode specified, it will return a OpenCV frame
that is either setup for normal, depth or infared.

- see Figure 1, for UML representation of OIFXWebCam

Other than that, I also implemented a function called loadWebCamTexture(), this would simply turn a OpenCV frame and turn it into
a texture, that an be then be applied to a model in OpenGL.

# Relevant Code Locations
- OIFXWebCam.cpp and OIFXWebCam.hpp is in include folder
  - include/OIFXWebCam.hpp
  - include/OIFXWebCam.cpp
- main code for assignment is in examples folder
  - examples/webcam_test/main.cpp

# Most Difficult Part
The most difficult part of this project was by far the setting up of OpenISS and Assignment-2. I did not
have much experience with c++ and even less experience with cmake. Despite the README.md given in the OpenISS
repo, it was still very confusing how to link everything and have it compile. It took many hours of learning how to 
work with cmake and looking at examples, cross-referencing with the cmake files given in the OpenISS, to have 
it finally compile.

\pagebreak
# Appendix
![device uml](device.png "device uml"){height=500px}
